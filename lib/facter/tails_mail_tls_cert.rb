Facter.add('tails_mail_tls_cert') do
  setcode do
    file = '/etc/letsencrypt/live/mail.tails.boum.org/cert.pem'
    File.exists?(file) ? File.read(file) : nil
  end
end
