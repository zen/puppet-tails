#!/bin/sh

# This script is called by the collect_build_environment jenkins-job-builder
# builder, defined in jenkins-jobs.git.

set -e
set -u
set -x

OUT_FILE_NAME="$1"

[ "$OUT_FILE_NAME" ] || exit 2
[ "$GIT_COMMIT" ] || exit 3
[ "$GIT_BRANCH" ] || exit 4
[ "$BUILD_NUMBER" ] || exit 5
[ "$WORKSPACE" ] || exit 6

BASE_BRANCH=$(head -n1 config/base_branch)
BASE_BRANCH_HEAD=$(git rev-parse "origin/${BASE_BRANCH}")

[ "$BASE_BRANCH_HEAD" ] || exit 7

OUT_FILE="build-artifacts/${OUT_FILE_NAME}"

. ${WORKSPACE}/tmp/recipient

{
    echo "UPSTREAMJOB_GIT_COMMIT=${GIT_COMMIT}"
    echo "UPSTREAMJOB_GIT_BRANCH=${GIT_BRANCH}"
    echo "UPSTREAMJOB_GIT_BASE_BRANCH=${BASE_BRANCH}"
    echo "UPSTREAMJOB_GIT_BASE_BRANCH_HEAD=${BASE_BRANCH_HEAD}"
    echo "UPSTREAMJOB_BUILD_NUMBER=${BUILD_NUMBER}"
    echo "UPSTREAMJOB_NAME=${JOB_NAME}"
} >> "${OUT_FILE}"

case "${GIT_BRANCH}" in
	origin/devel|origin/master|origin/stable|origin/testing)
		true
	;;
	*)
		# Collect necessary notification variable for the test and reproducible child job.
		[ "$NOTIFY_TEST_TO" ] || NOTIFY_TEST_TO=""
		echo "NOTIFY_TEST_TO=${NOTIFY_TEST_TO}" >> "${OUT_FILE}"
		[ "$NOTIFY_BUILD_TO" ] || NOTIFY_BUILD_TO=""
		echo "NOTIFY_BUILD_TO=${NOTIFY_BUILD_TO}" >> "${OUT_FILE}"
	;;
esac
