#!/usr/bin/python3

import argparse
import jenkins
import time


def main():
    parser = argparse.ArgumentParser(
        description="Ensure a Jenkins node is not marked as temporarily offline"
    )
    parser.add_argument("--orchestrator-url",
                        type=str,
                        action="store",
                        required=True)
    parser.add_argument("--node-name", type=str, action="store", required=True)
    parser.add_argument("--api-user", type=str, action="store", required=True)
    parser.add_argument("--api-token-file",
                        type=str,
                        action="store",
                        required=True)
    args = parser.parse_args()

    with open(args.api_token_file) as api_token_file:
        api_token = api_token_file.read().rstrip()
    server = jenkins.Jenkins(args.orchestrator_url,
                             username=args.api_user,
                             password=api_token)
    # We should not need this check, but .enable_node() is buggy:
    #  - It's implemented in a racy way.
    #  - As Zen Fu wrote on
    #    https://gitlab.tails.boum.org/tails/sysadmin/-/issues/10601#note_152683,
    #    "Enabling an offline node will mark it as temporarilyOffline if it was
    #     not marked as such before".
    if server.get_node_info(args.node_name)["temporarilyOffline"]:
        print("Node is marked as temporarily offline, enabling it.")
        server.enable_node(args.node_name)
        # Probably unneeded, but does not hurt:
        #  - 46ad3c627b16c54342bf6a380116a4c223e1450e
        #  - https://gitlab.tails.boum.org/tails/sysadmin/-/issues/10601#note_152683
        while server.get_node_info(args.node_name)["temporarilyOffline"]:
            print("Node is still temporarily offline, waiting…")
            time.sleep(1)
    else:
        # This should never happen in theory:
        # https://gitlab.tails.boum.org/tails/sysadmin/-/issues/10601#note_152683
        # … but better safe than sorry.
        print("Node is not marked as temporarily offline, skipping.")


if __name__ == "__main__":
    main()
