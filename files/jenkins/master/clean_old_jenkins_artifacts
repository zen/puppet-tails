#!/usr/bin/ruby
#
# Ensure to keep only n occurences of a Jenkins Build_Tails_ISO_* job
# artifacts in a given time range.
#
# If keeping more artifacts is necessary, this might be achieved by adding a
# new entry in the `BACKUPS` array in the form:
#
#   "Name" => {
#      'range_start' => Range_start,
#      'range_end'   => Range_end,
#      'keep'        => Number_to_keep,
#      range_includes_end_value => true|false,
#   }
#
# The Number_to_keep newest sets of artifacts within the specified
# range are kept.
#
# Ranges are expressed with Time objects.
# Take care to define ranges that don't overlap with others.
# You may use the NOW and TODAY constants in your range definitions.

require 'optparse'
require 'fileutils'

# Constants

NOW   = Time.now
TODAY = Time.local(NOW.year, NOW.month, NOW.day)

EXTS = [".apt-sources",
        ".buildlog",
        ".build-manifest",
        ".img",
        ".iso",
        ".packages"]

BACKUPS = Hash[
  "Today" => {
    'range_start' => TODAY,
    'range_end'   => TODAY + (60 * 60 * 24) - 1,
    'keep'        => 5,
    'range_includes_end_value' => true,
  },
]

(1..9).each do |n|
  BACKUPS["#{n} day(s) ago"] = {
    'range_start' => TODAY - (60 * 60 * 24 * n),
    'range_end'   => TODAY - (60 * 60 * 24 * (n - 1)) - 1,
    'keep'        => 1,
    'range_includes_end_value' => true,
  }
end

(1..5).each do |n|
  BACKUPS["#{n} week(s) ago"] = {
    'range_start' => TODAY - (60 * 60 * 24 * 7 * n),
    'range_end'   => TODAY - (60 * 60 * 24 * 7 * (n - 1)),
    'keep'        => 1,
    'range_includes_end_value' => false,
  }
end

# Functions

def time_in_range(time, range_start, range_end, range_includes_end_value)
  if range_includes_end_value
    time >= range_start and time <= range_end
  else
    time >= range_start and time < range_end
  end
end

def isos_to_keep(isos, options)
  remainings = []
  BACKUPS.each do |name,definition|
    chosen = isos.select {|iso|
      time_in_range(
                    iso['mtime'],
                    definition['range_start'],
                    definition['range_end'],
                    definition['range_includes_end_value']
                    )
    }
      .sort{|a,b| b['mtime'] <=> a['mtime'] }
      .first(definition['keep'])

    puts "Keeping #{chosen.inspect} for #{name} range." if options[:dryrun]
    remainings.concat(chosen)
  end
  remainings
end

def find_all_isos(options)
  isos = []
  # Use `**` in the regexp to avoid following symlinks in
  # $topdir/builds/ and return each ISO twice.
  Dir.glob("#{options[:topdir]}/builds/**/archive/build-artifacts/*.iso") do |iso_filename|
    isos << Hash[ 'filename' => iso_filename.gsub(/\.iso$/, ''), 'mtime' => File.mtime(iso_filename) ]
  end
  isos
end

# Main

options = {}

optparse = OptionParser.new do |opts|
  opts.banner = "Usage: clean_old_jenkins_artifacts.rb -t /path/to/artifacts/topdir [-n]"

  opts.on( '-t', '--topdir DIR', 'Top level directory where the job artifacts are stored.' ) do |dir|
    options[:topdir] = dir
  end

  options[:dryrun] = false
  opts.on( '-n', '--dry-run', 'Tell what would happen and exit.' ) do
    options[:dryrun] = true
  end

  opts.on( '-h', '--help', 'This help.' ) do
    puts opts
    exit
  end
end

optparse.parse!

if not options[:topdir]
  puts "You need to specify the top-level directory of artifacts with the -t option."
  exit 1
end

all_isos = find_all_isos(options)

isos_to_delete = all_isos - isos_to_keep(all_isos, options)
isos_to_delete.each do |iso_to_delete|
  if options[:dryrun]
    puts "Deleting #{iso_to_delete['filename']} (#{iso_to_delete['mtime']})"
  end
  artifacts = EXTS.map {|e| iso_to_delete['filename'] + e}
  FileUtils.rm(artifacts, :force => true) if not options[:dryrun]
end

