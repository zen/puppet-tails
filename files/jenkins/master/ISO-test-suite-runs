#! /usr/bin/perl

# Count runs of jobs whose name starts with "test_Tails_ISO_".

# Usage:
#
#    ISO-test-suite-runs GLOBAL_BUILD_STATS_RESULT_FILE \
#                        [SINCE_EPOCH [BEFORE_EPOCH]
#
# e.g.:
#
#    ISO-test-suite-runs jobResults-2015-11.xml 1449010217456

use strict;
use warnings FATAL => 'all';
use 5.10.1;

use Mojo::DOM;
use Path::Tiny;

@ARGV == 1 || @ARGV == 2 || @ARGV == 3 || exit 1;

my $file   = path(shift);
my $since  = shift;
my $before = shift;

sub drop_ms_from_epoch {
    my $epoch_with_ms = shift;

    my ($epoch_without_ms) = ($epoch_with_ms =~ m{([[:digit:]]+)[[:digit:]]{3}\z});
    return $epoch_without_ms;
}

Mojo::DOM->new($file->slurp_utf8)
    ->find('jbr n')
    ->grep( sub { $_->text =~ m{\Atest_Tails_ISO_} })
    ->map ( sub { $_->parent })
    ->grep( sub {
        return 1 unless defined $since;
        $since < drop_ms_from_epoch($_->at('d time')->text)
    })
    ->grep( sub {
        return 1 unless defined $before;
        drop_ms_from_epoch($_->at('d time')->text) < $before
    })
    ->each ( sub {
        my $job_name = $_->at('n')->text;
        my $run_id   = $_->at('nb')->text;
        say "https://nightly.tails.boum.org/$job_name/builds/"
            . "$run_id/archive/build-artifacts/cucumber.json";
    });
