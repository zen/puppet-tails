#!/usr/bin/python3

import requests

headers = {
    "X-API-Key": open("/etc/pdnsapi.secret", "r").read().replace("\n", ""),
}


def validate_ip(s):
    a = s.split(".")
    if len(a) != 4:
        return False
    for x in a:
        if not x.isdigit():
            return False
        i = int(x)
        if i < 0 or i > 255:
            return False
    return True


counter = 0
records = ""

for ip in open("./dl.txt"):
    ip = ip.rstrip()
    counter += 1
    if validate_ip(ip):
        records += ', {"content": "' + ip + '", "disabled": false}'
    else:
        print("you fed me garbage on line " + str(counter))

records = records[2:]

rrdata = (
    '{"rrsets": [ {"name": "dl.amnesia.boum.org.", "type": "A", "changetype": "REPLACE", "records": [ '
    + records
    + ' ], "ttl": 3600 , "type": "A" } ] }'
)
response = requests.patch(
    "http://127.0.0.1:8081/api/v1/servers/localhost/zones/amnesia.boum.org",
    headers=headers,
    data=rrdata,
)
