#!/usr/bin/python3
#
# Podman Weblate service and container management.
#
# Run this script to see command-line options.


import click
import subprocess


SUDO = ['/usr/bin/sudo', '-u', 'weblate', '--preserve-env=XDG_RUNTIME_DIR']
SYSTEMCTL = ['/bin/systemctl', '--user']
PODMAN = ['/usr/bin/podman']
ENV = {'XDG_RUNTIME_DIR': '/run/user/2000000'}
SERVICE = 'podman-weblate.service'
CONTAINER = 'weblate'


def _debug():
    return click.get_current_context().parent.params['debug']


def _sudo(cmd):
    full_cmd = SUDO + cmd
    if _debug():
        click.echo(click.style("debug: Environment: " + str(ENV), fg='yellow'))
        click.echo(click.style("debug: Command: " + str(full_cmd), fg='yellow'))
    p = subprocess.Popen(full_cmd, stdout=subprocess.PIPE, env=ENV, text=True)
    for l in p.stdout:
        click.echo(l[:-1])
    p.stdout.close()


def _systemctl(cmd):
    _sudo(SYSTEMCTL + cmd)


def _podman(cmd):
    _sudo(PODMAN + cmd)


@click.option('-d', '--debug', is_flag=True, help='Output debugging information')
@click.group()
def cli(debug):
    pass


@cli.command(help="Start the container")
def start():
    _systemctl(['start', SERVICE])


@cli.command(help="Stop the container")
def stop():
    _systemctl(['stop', SERVICE])


@cli.command(help="Restart the container")
def restart():
    _systemctl(['restart', SERVICE])


@cli.command(help="Get status of service")
def status():
    _systemctl(['status', SERVICE])


@cli.command(help="View container logs")
@click.option('-f', '--follow', is_flag=True, help='Follow logs output')
@click.option('--tail', type=int, help='Show only the N last log lines')
def logs(follow, tail):
    cmd = ['logs']
    if tail:
        cmd += [f'--tail={tail}']
    if follow:
        cmd += ['-f']
    _podman(cmd + [CONTAINER])


@cli.command(help="Show running containers")
def ps():
    _podman(['ps'])


if __name__ == '__main__':
    cli()
