#!/usr/bin/env python3

'''Check and enforce permissions for Weblate.

By default, this script checks Weblate permissions against the ones described
in a config file. Use `--enforce` to actually apply the permissions from the
config file to Weblate.


Objects that are checked/modified
---------------------------------

Only the following Weblate objects are checked and possibly modified by this
script:

    - All groups and roles defined in the config file.
    - All users with permissions defined in the config file.
    - All users that are not in the config file but are active in Weblate.

We deliberately avoid going through inactive users because those include
"deleted-XXX" users, and that is because deactivating+renaming is the way
Weblate handles user deletion. Add an inactive user to the config file to force
it to be checked.

The script will warn you and (if `--enforce` is passed) will create the roles
and groups that are defined but do not yet exist in Weblate. Removing a group
or role from the config file will not delete it. Add a `__deleted` flag to a
group or role to actually delete it.


Configuration file
------------------

The script searches for a config file named `weblate_permission.yaml` in `.`,
`..` or `../config`.

The YAML config file has four section: `groups`, `roles`, `user` and
`superusers`.

In the examples below, the Capitalized strings are the ones you should change
according to your needs. Strings in lowercase should not be changed as they are
words reserved by the script.

    group:
      Group 1:
        __deleted: False   # Delete the group? (default: False)
        codes:             # List of languages codes. Set to a list with a
          - de             #   single item with value `_all` to select all
          - ...            #   languages.

        projects:          # List of projects to set for this group. Set to a
          - Project 1      #   list with a single item with value `_all` to
          - ...            #   select all projects.

        roles:             # List of roles to set for this group.
          - Role 1
          - ...

        users:             # List of users to add to this group.
          - User 1
      Group 2:
        ...

    roles:
      Role 1:              # List of roles. Set to a list iwth a single item
        - suggestion.add   #   `__deleted` to delete this role.
        - ...
      Role 2:
        ...

    users:
      User 1:
        is_active: True    # Is the user active? (default: True)
        groups:            # List of group names. Add an item with value
          - Group 1        #   `__default` to add this user to all groups
      User 2:              #   defined in the `__default` user template
        ...                #   (see below for more info).

    superusers:
      - User1              # List of users that should be superusers.


The `__default` user template
-----------------------------

For all users that are not defined in the config file, the `__default` user
template is applied. You can define such template in the top level and/or in
the `users` section of each group.

When no `__default` user template is defined, the following defaults are used
as a fallback:

    - is_active: True
    - groups: []

Every user that is active in Weblate but not defined in the config file will be
checked and (if `--enforce`) added to all the groups defined for the
`__default` user template. To override this behavior you need to explicitly add
the user to the `users` section.


.maintenance file
-----------------

If a `.maintenance` files lives next to the config file, the script falls back
to "check mode". This should give service maintainers the freedom to do
maintenance without the script interfering in the work. To run the script in
"enforce mode" even when the `.maintenance` file exists, you have to pass the
`--enforceMaintenance` option.
'''

import collections
import copy
import logging
import logging.config
import operator
import yaml
import tailsWeblate
from weblate.auth.models import Group, Language, Permission, Project, Role, User

from weblate.auth.data import (
    SELECTION_ALL,
    SELECTION_ALL_PROTECTED,
    SELECTION_ALL_PUBLIC,
    SELECTION_COMPONENT_LIST,
    SELECTION_MANUAL,
)


class ConfigNotFoundError(Exception):
    pass


class Config:
    def __init__(self, paths):
        self.paths = paths
        self.config = None

    def load(self):
        for path in self.paths:
            if path.exists():
                with path.open() as f:
                    self.config = yaml.safe_load(f)
                self.path = path
                break
        else:
            self.config = None
            raise ConfigNotFoundError("No config was found, searched in: %s", self.paths)

    def __getattr__(self, item):
        if not self.config:
            self.load()
        return self.config.get(item)


PERMISSIONS_NAME = "weblate_permissions.yaml"

logger = logging.getLogger(__name__)
auditlogger = logger.getChild('audit')


def format_diff(actual, expected):
    ''' return a diff kind of output of actual and expect.'''
    ret = ["+++ expected", "--- actual"]
    for i in sorted(actual | expected):
        if i not in actual:
            ret.append("+ {}".format(i))
        if i not in expected:
            ret.append("- {}".format(i))
    return ret


def check(actual, expected, logline, logfunc, diff=True):
    ''' check if actual == expected, if not print logline.
        - diff=True will print a diff like output about the differences in lists/sets'''
    if actual == expected:
        return True
    if diff:
        logfunc(logline+"\n "+"\n ".join(format_diff(actual, expected)))
    else:
        logfunc(logline+" ({} != {})".format(actual, expected))
    return False


def element_check(element, attr, expected, logline, logfunc, diff=True):
    return check(getattr(element, attr), expected, logline, logfunc, diff)


def check_and_enforce(element, attr, expected, logline, logfunc, diff=True):
    if not element_check(element, attr, expected, logline, logfunc, diff):
        setattr(element, attr, expected)
        return False
    return True


class DBQuery:
    def __init__(self, query, attr, pool):
        self.query = query
        self.attr = attr
        self.pool = pool

    def check(self, expected, logline, logfunc, diff=True):
        func = operator.attrgetter(self.attr)
        actual = set(func(i) for i in self.query.all())
        return check(actual, expected, logline, logfunc, diff)

    def enforce(self, expected):
        self.query.set(self.pool.filter(**{self.attr+"__in":expected}), clear=True)

    def check_and_enforce(self, expected, logline, logfunc, diff=True):
        if not self.check(expected, logline, logfunc, diff):
            self.enforce(expected)
            return False
        return True


def weblate_permission(expected, enforce):

    # replace lists with sets, as order is not relevant in this context
    for k,v in expected.roles.items():
        expected.roles[k] = set(v)

    roles = set(k for k,v in expected.roles.items() if not '__deleted' in v)
    groups = set(k for k,v in expected.groups.items() if not v.get('__deleted'))

    if expected.users is None:
        expected.config['users'] = dict()

    if expected.superusers is None:
        expected.config['superusers'] = list()

    if not '__default' in expected.users:
        expected.users['__default']={'is_active': True, 'groups':set()}

    for v in expected.users.values():
        v["groups"] = set(v.get("groups",[]))

    # replace lists with sets, as the ordering does not contain information
    # add all implicit users to __default groups (not defined under users)
    for g,v in sorted(expected.groups.items(), key=lambda i: i[0]):
        if v is None:
            v = {}
            expected.groups[g] = v
        v['roles'] = set(v.get('roles', []))
        if v['roles'] - roles:
            logger.warning('Undefined roles are used in {}\n {}'.format(g,'\n '.join(sorted(v['roles'] - roles))))
        v['codes'] = set(v.get('codes', []))
        v['projects'] = set(v.get('projects', []))
        if v.get('__deleted'):
            continue
        for user in v.get('users', []):
            if user not in expected.users:
                expected.users[user] = {'is_active': True, 'groups': set(('__default',))}
            expected.users[user]['groups'].add(g)

    # replace lists with sets, as the ordering does not contain information
    # replace __default group with all default groups
    for u,v in sorted(expected.users.items(), key=lambda i: i[0]):
        if u == '__default':
            continue
        if '__default' in v["groups"]:
            v["groups"].remove('__default')
            v["groups"] |= expected.users['__default']['groups']
        if v['groups'] - groups:
            logger.warning('Undefined groups are used in {}\n {}'.format(u,'\n '.join(sorted(v['groups'] - groups))))

    attribute = operator.attrgetter("check")
    check_func = element_check
    logfunc = logger.warning
    if enforce:
        attribute = operator.attrgetter("check_and_enforce")
        check_func = check_and_enforce
        logfunc = auditlogger.info

    # Check roles
    for name, e in sorted(expected.roles.items(), key=operator.itemgetter(0)):
        i = Role.objects.filter(name=name).first()
        if i:
            dbquery = DBQuery(i.permissions, 'codename', Permission.objects)
            if '__deleted' in e:
                if enforce:
                    dbquery.check(set(), 'Delete role({name}) with permissions:'.format(name=name), logfunc)
                    i.delete()
                else:
                    logger.warning('role({name}) found, that is marked as deleted.'.format(name=name))
                continue

            if not attribute(dbquery)(e, "Permission mismatch for role({name})".format(name=name), logfunc):
                if enforce:
                    auditlogger.info('Save role({name})'.format(name=name))
                    i.save()
        else:
            if '__deleted' in e:
                continue
            if enforce:
                auditlogger.info('Create role({name})'.format(name=name))
                role = Role.objects.create(name=name)
                role.permissions.set(Permission.objects.filter(codename__in=e), clear=True)
                role.save()
            else:
                logger.warning('Missing role({name})'.format(name=name))


    # Check groups
    for name, e in sorted(expected.groups.items(), key=operator.itemgetter(0)):
        i = Group.objects.filter(name=name).first()
        if i:
            changed = False
            dbquery = DBQuery(i.roles, 'name', Role.objects)

            if e.get('__deleted'):
                if not enforce:
                    logger.warning('Additional group({name}) detected'.format(name=name))
                    continue
                auditlogger.info('Delete group({name})'.format(name=name))
                dbquery = DBQuery(i.roles, 'name', Role.objects)
                dbquery.check(set(), "Roles for group({name})".format(name=name), logfunc)
                if i.language_selection == SELECTION_MANUAL:
                    dbquery = DBQuery(i.languages, 'code', Language.objects)
                    dbquery.check(set(), "Codes for group({name})".format(name=name), logfunc)
                if i.project_selection == SELECTION_MANUAL:
                    dbquery = DBQuery(i.projects, 'name', Project.objects)
                    dbquery.check(set(), "Projects for group({name})".format(name=name), logfunc)
                i.delete()
                auditlogger.info('group({name}) deleted.'.format(name=name))
                continue

            dbquery = DBQuery(i.roles, 'name', Role.objects)
            if not attribute(dbquery)(e['roles'], "Roles mismatch for group({name})".format(name=name), logfunc):
                changed = True

            if '__all' in e['codes']:
                if not check_func(i, 'language_selection', SELECTION_ALL, "Group {name} should select every languages.".format(name=name), logfunc, False):
                    changed = True
            else:
                if not check_func(i, 'language_selection', SELECTION_MANUAL, "Group {name} should have a manual languages selection.".format(name=name), logfunc, False):
                    changed = True

                dbquery = DBQuery(i.languages, 'code', Language.objects)
                if not attribute(dbquery)(e['codes'], "Code mismatch for group({name})".format(name=name), logfunc):
                    changed = True

            if '__all' in e['projects']:
                if not check_func(i, 'project_selection', SELECTION_ALL, "Group {name} should select every project.".format(name=name), logfunc, False):
                    changed = True
            else:
                if not check_func(i, 'project_selection', SELECTION_MANUAL, "Group {name} should have a manual project selection.".format(name=name), logfunc, False):
                    changed = True
                dbquery = DBQuery(i.projects, 'name', Project.objects)
                if not attribute(dbquery)(e['projects'], "Project mismatch for group({name})".format(name=name), logfunc):
                    changed = True
            if changed and enforce:
                auditlogger.info('Save group({name})'.format(name=name))
                i.save()
        else:
            if e.get('__deleted'):
                continue
            if enforce:
                auditlogger.info('Create group({name})'.format(name=name))
                args = {'name':name,
                        'project_selection': SELECTION_ALL,
                        'language_selection': SELECTION_ALL,
                }
                group = Group.objects.create(**args)
                group.roles.set(Role.objects.filter(name__in=e['roles']), clear=True)
                if not '__all' in e['projects']:
                    group.project_selection = SELECTION_MANUAL
                    group.projects.set(Project.objects.filter(name__in=e['projects']), clear=True)
                if not '__all' in e['codes']:
                    group.language_selection = SELECTION_MANUAL
                    group.languages.set(Language.objects.filter(code__in=e['codes']), clear=True)
                group.save()

            else:
                logger.warning('Missing group({name})'.format(name=name))


    # Check users
    for i in sorted(User.objects.all(),key=lambda i:i.username.lower()):
        changed = False

        if not i.is_active and i.username not in expected.users:
            # Ignore inactive users, if they are not listed explictitly
            continue

        e = expected.users.get(i.username, expected.users['__default'])

        dbquery = DBQuery(i.groups, 'name', Group.objects)
        if not attribute(dbquery)(e['groups'], "Group mismatch for user({username})".format(username=i.username), logfunc):
            changed = True
        if not check_func(i, 'is_superuser', i.username in expected.superusers, "Wrong superuser status for {username}".format(username=i.username), logfunc, False):
            changed = True
        if not check_func(i,'is_active', e.get('is_active'), "Wrong is_active status for {username}".format(username=i.username), logfunc, False):
            changed = True
        if changed and enforce:
            auditlogger.info('Save user({username})'.format(username=i.username))
            i.save()


def commandline():
    import argparse
    import os
    import pathlib

    global logger
    global auditlogger

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-v", "--verbose",
        action='store_true',
        help="verbose logging.")
    parser.add_argument(
        "--enforce",
        action='store_true',
        help="enforce permissions.")
    parser.add_argument(
        "--enforceMaintenance",
        action='store_true',
        help="enforce permissions even in maintanance mode.")
    args = parser.parse_args()

    path = pathlib.Path(os.path.realpath(__file__))
    config = Config([pathlib.Path('.'+PERMISSIONS_NAME), path.with_name(PERMISSIONS_NAME), path.parent.with_name("config")/PERMISSIONS_NAME])
    config.load()

    logging.config.fileConfig([str(config.path.with_name('weblate_permissions.conf'))])
    logger = logging.getLogger('')
    logger.level = logging.INFO
    auditlogger = logger.getChild('audit')

    if args.verbose:
        logger.level = logging.DEBUG

    if args.enforceMaintenance:
        args.enforce = True

    if args.enforce:
        if not args.enforceMaintenance and config.path.with_name('.maintenance').exists():
            args.enforce = False
            logger.warning('System is in maintance mode - not enforcing permissions, just run check mode.\nUse --enforceMaintanance to overwrite.')
    if args.enforce:
        logger.info('Run in enforce mode.')
    else:
        logger.info('Run in check mode.')
    try:
        weblate_permission(config, args.enforce)
    except:
        logger.exception('Failed with exception.')
        raise
    logger.info('Finish run.')

if __name__ == "__main__":
    commandline()
