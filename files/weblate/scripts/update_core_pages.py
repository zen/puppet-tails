#!/usr/bin/env python3

"""Update list of core pages in Weblate.

Tails maintains a list of "core pages", which are pages from its website that
should be prioritized for translation:

    https://tails.boum.org/contribute/l10n_tricks/core_po_files.txt

Weblate allows for creating "component lists" to appear as options in users'
dashboards. This script updates Weblate's "core pages" component list with the
latest data from the Tails Git repository.
"""

import logging
import os

import tailsWeblate


SCRIPT_DIR = os.path.dirname(os.path.realpath(__file__))
COMPONENT_BASEPATH = "wiki/src"
CORE_FILES_PATH = "/wiki/src/contribute/l10n_tricks/core_po_files.txt"

logging.config.fileConfig(SCRIPT_DIR + '/../config/updateCorePages.conf')
logger = logging.getLogger(__name__)


def remove_components(components, component_list):
    for component_filemask in components:
        try:
            component = tailsWeblate.subProject(component_filemask)
            logger.info("Removing component from core pages: {}".format(component_filemask))
            # non-core pages should not be prioritized
            component.priority = '100'  # default value
            component.save()
            component_list.components.remove(component)
        except:
            logger.exception("Removing compontent {} failed.".format(component_filemask))
            raise


def add_components(components, component_list):
    for component_filemask in components:
        try:
            component = tailsWeblate.subProject(component_filemask)
            logger.info("Adding component to core pages: {}".format(component_filemask))
            # core pages should be offered first for translation
            component.priority = '80'  # default is 100, lower values have higher priority
            component.save()
            component_list.components.add(component)
        except:
            logger.exception("Adding compontent {} failed.".format(component_filemask))
            raise


def get_component_filemask(line:str) -> str:
    name = line[2:].strip()
    if name.endswith(".mdwn"):
        name = name[:-5]
    return os.path.join(COMPONENT_BASEPATH, "{}.*.po".format(name))


def get_core_pages():
    path = tailsWeblate.models.Component.objects.first().full_path + CORE_FILES_PATH
    with open(path, "r") as f:
        return set((get_component_filemask(i) for i in f.readlines()))


def update_core_pages():
    component_list = tailsWeblate.models.ComponentList.objects.filter(name="core pages").first()
    core_pages = get_core_pages()
    weblate_core_pages = set(component_list.components.all().values_list('filemask', flat=True))

    components_to_remove = weblate_core_pages - core_pages
    remove_components(components_to_remove, component_list)

    components_to_add = core_pages - weblate_core_pages
    add_components(components_to_add, component_list)


if __name__ == "__main__":
    logger.info("Starting to update core pages component list...")
    update_core_pages()
    logger.info("Finished updating core pages component list.")
