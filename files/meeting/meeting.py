#!/usr/bin/python3
# Copyright (c) 2018 Muri Nicanor <muri@immerda.ch>. All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#  1. Redistributions of source code must retain the above copyright notice,
#     this list of conditions and the following disclaimer.
#  2. Redistributions in binary form must reproduce the above copyright
#     notice, this list of conditions and the following disclaimer in the
#     documentation and/or other materials provided with the distribution.
#  3. Neither the name of the copyright holder nor the names of its
#     contributors may be used to endorse or promote products derived from this
#     software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

import datetime
from datetime import timedelta
import argparse
import logging
import smtplib
import socket
from string import Template
from email.mime.text import MIMEText
from email.utils import parseaddr
from pathlib import Path
import sys
import requests


LOG_FORMAT = "%(asctime)-15s %(levelname)s %(message)s"
log = logging.getLogger()


def next_meeting_date(mon=None, meetingday=None, skip_friday_to_sunday=False):
    """Calculate the date of the next meeting.
    The default is the 3rd of the next month.
    """
    mon = mon or 0
    meetingday = meetingday or 3
    today_date = datetime.date.today()

    # if no specific month was given, check if the date
    # is in this months future- if not, take the next
    # month
    if mon == 0:
        if today_date.day >= meetingday:
            if today_date.month == 11:
                mon = 12
            else:
                mon = (today_date.month + 1) % 12
        else:
            mon = today_date.month

    # if the month is in the past, calculate for the month next year
    if mon < today_date.month:
        today_date = today_date.replace(year=today_date.year + 1)

    # calulate the meetingdate
    try:
        next_meeting = today_date.replace(month=mon, day=meetingday)
    except ValueError as e:
        sys.exit("The month/day combination month: {}, "
                 "day: {} is invalid: {}".format(mon, meetingday, e))

    if skip_friday_to_sunday:
        # if the meetingday falls on a Friday, Saturday, or Sunday,
        # then choose the day three days after
        if next_meeting.weekday() >= 4:
            next_meeting += timedelta(days=3)

    return next_meeting


def email_body(date, append_web_page=None):
    """Generate the body of the email from the template"""
    body = ''
    try:
        with open(template) as t:
            src = Template(t.read())
    except PermissionError as e:
        sys.exit("Could not open {}: {}".format(template, e))
    d = {'date': date.strftime("%A %d. %B %Y")}
    body = src.substitute(d)
    if append_web_page:
        body += requests.get(append_web_page).text
    return body


def email_subject(subject, date, append_date_to_subject):
    humanreadabledate = date.strftime("%A %B %d")
    if append_date_to_subject:
        return "{}{}".format(subject, humanreadabledate)
    return subject


def send_email(fromaddress, subject, append_date_to_subject, date, address, send=True, append_web_page=None):
    """Send an email using sendmail"""
    message = email_body(date, append_web_page)
    email = MIMEText(message)
    email['Subject'] = email_subject(subject, date, append_date_to_subject)
    email['From'] = fromaddress
    email['To'] = address
    if send:
        log.debug("email: %s", email)
        try:
            s = smtplib.SMTP('localhost')
            s.sendmail(email['From'], email['To'], email.as_string())
            s.quit()
        except socket.error as e:
            print("Could not connect to mailserver on localhost. "
                  "Email wasn't sent to {}: {}".format(address, e))
    else:
        print(email)


# the main function parses and validates the arguments and
# then either prints the info to stdout or calls other
# methods to generate an email body or send emails
if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-m", "--month", type=int,
                        help="Set the month (default is next month).",
                        choices=[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12])
    parser.add_argument("-r", "--reminders", type=str,
                        help="Set which days before a meeting should be a "
                        "'reminder day'.")
    parser.add_argument("-d", "--day", type=int,
                        help="Set the day the meeting should happen.")
    parser.add_argument("--skip-friday-to-sunday", default=False,
                        action="store_true",
                        help="Consider all days as valid meeting days")
    parser.add_argument("-t", "--template", type=str,
                        help="Pass a template for the email.", required=True)

    parser.add_argument("-a", "--addresses", type=str, required=True,
                        help="List of addresses the reminder email should be "
                        "sent to (seperated by comma)")
    parser.add_argument("-f", "--from", type=str, required=True,
                        help="Address email is send from",
                        default="noreply@tails.boum.org")
    parser.add_argument("-s", "--subject", type=str, required=True,
                        help="Subject of the email")
    parser.add_argument("--append-date-to-subject",
                        default=False,
                        action="store_true",
                        help="Append the meeting date to the subject")
    parser.add_argument("--append-web-page", type=str,
                        help="Append the contents of this web page" +
                        " to the email body")
    parser.add_argument("--print-schedule", action="store_true",
                        help="Display the meeting and reminder schedule" +
                        " but don't generate reminder email")
    parser.add_argument("--dry-run", action="store_true",
                        help="Don't send email; instead, display it")
    parser.add_argument("--debug", action="store_true", help="debug output")

    args = parser.parse_args()

    if args.debug:
        logging.basicConfig(level=logging.DEBUG, format=LOG_FORMAT)
    else:
        logging.basicConfig(level=logging.INFO, format=LOG_FORMAT)

    log.debug("Args:\n%s", args)

    fromaddress = getattr(args, 'from')
    subject = args.subject
    template = args.template
    if not Path(template).is_file():
        sys.exit("{} is not a file".format(template))

    # make a list of the addresses given in --to
    # for every address we test if it is rfc-822 conform
    # and if it contains an '@'
    if args.addresses:
        toaddresses = [address for address in args.addresses.split(',')]
        for address in toaddresses:
            if parseaddr(address) == ('', '') or '@' not in address:
                sys.exit("{} is not a valid email address.".format(address))

    # calculate the next meeting date
    date = next_meeting_date(args.month, args.day, args.skip_friday_to_sunday)
    log.debug("Next meeting: %s", date)

    # make a list of dates that should be reminderdates
    reminders = []
    if args.reminders:
        for item in [int(item) for item in args.reminders.split(',')]:
            reminders.append(date - timedelta(days=item))

    log.debug("Reminder days: %s", reminders)

    # is today a reminderday?
    remindertoday = date.today() in reminders

    # print the date of the next meeting(s) to stdout
    # if one or more reminder dates are set, it also lists those
    if args.print_schedule:
        print("The next occurrence of this event will happen on {}".format(
            date.strftime("%A %d. %B %Y")))
        for reminder in sorted(reminders):
            tail = " - thats today!" if date.today() == reminder else "."
            msg = "One reminder would be sent on {}{}".format(
                    reminder.strftime("%A %d. %B %Y"), tail)
            print(msg)
    else:
        # if the current date is a reminderday or no reminderdays are specified,
        # either print an email to stdout or send an email using sendmail
        if remindertoday or not reminders:
            for address in toaddresses:
                send_email(fromaddress, subject,
                           args.append_date_to_subject,
                           date, address, send=not args.dry_run,
                           append_web_page=args.append_web_page)
