#!/bin/bash

# Usage: tails-publish-tagged-apt-snapshot SNAPSHOTS_DIRECTORY TAG
#
# Publishes a tagged APT snapshot: gives it the right ownership and permissions,
# and moves it to the directory where it's served over HTTP.
#
# Beware: this script is run as root, via sudo, by an unprivileged user.

set -e
set -u
set -o pipefail

TIME_BASED_SNAPSHOTS_USER='reprepro-time-based-snapshots'
TIME_BASED_SNAPSHOTS_GROUP='reprepro-time-based-snapshots'
TAGGED_SNAPSHOTS_USER='reprepro-tagged-snapshots'
TAGGED_SNAPSHOTS_GROUP='reprepro-tagged-snapshots'
TAGGED_SNAPSHOTS_HOME=$(getent passwd "$TAGGED_SNAPSHOTS_USER" | cut -d':' -f6)
TAGGED_SNAPSHOTS_REPOSITORIES="${TAGGED_SNAPSHOTS_HOME}/repositories"

error() {
   echo "$@" >&2
   exit 1
}

[ $# -eq 2 ] || exit 1

SNAPSHOT_DIRECTORY="$1"
TAG="$2"

[ -d "$SNAPSHOT_DIRECTORY" ] || error "'$SNAPSHOT_DIRECTORY' is not a directory"
[ "$(stat --format='%U' "$SNAPSHOT_DIRECTORY")" = "$TIME_BASED_SNAPSHOTS_USER" ]  \
    || error "'$SNAPSHOT_DIRECTORY' is not owned by user '$TIME_BASED_SNAPSHOTS_USER'"
[ "$(stat --format='%G' "$SNAPSHOT_DIRECTORY")" = "$TIME_BASED_SNAPSHOTS_GROUP" ] \
    || error "'$SNAPSHOT_DIRECTORY' is not owned by group '$TIME_BASED_SNAPSHOTS_GROUP'"
echo "$TAG" | grep -q -E --line-regexp '[0-9a-z.-]+' \
    || error "'$TAG' is not a valid tag name"
[ ! -e "${TAGGED_SNAPSHOTS_REPOSITORIES}/${TAG}" ] \
    || error "A tagged snapshot already exists in '${TAGGED_SNAPSHOTS_REPOSITORIES}/${TAG}'"

chown -R "${TAGGED_SNAPSHOTS_USER}:${TAGGED_SNAPSHOTS_GROUP}" \
      "$SNAPSHOT_DIRECTORY"
chmod -R go+rX "$SNAPSHOT_DIRECTORY"
mv "$SNAPSHOT_DIRECTORY" "${TAGGED_SNAPSHOTS_REPOSITORIES}/${TAG}"
