# Manage Jenkins plugins
class tails::jenkins::master::plugins {

  # lint:ignore:140chars -- SHA512

  jenkins::plugin { 'antisamy-markup-formatter':
    version       => '155.v795fb_8702324',
    digest_string => '97358c0517a6451d1333b60b87358a91befa2a22d12aa62bcd631ac3f464bed2',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'instance-identity',
      'javax-activation-api',
      'javax-mail-api',
      'sshd',
    ],
  }

  jenkins::plugin { 'apache-httpcomponents-client-4-api':
    version       => '4.5.13-138.v4e7d9a_7b_a_e61',
    digest_string => '468eb1f110b744b5c87cb6a5b619781f08b2200c9ca02e4456820e7220195aa8',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'instance-identity',
      'javax-activation-api',
      'javax-mail-api',
    ],
  }

  jenkins::plugin { 'bootstrap5-api':
    version       => '5.2.1-3',
    digest_string => '42f150cefd03b878bccb498218de68c526b1ca3def60dcd3901caadbee881f5a',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'font-awesome-api',
      'instance-identity',
      'popper2-api',
    ],
  }

  jenkins::plugin { 'bouncycastle-api':
    version       => '2.26',
    digest_string => '2b9ec2bd9bee111ff985fec297e1d78c20812daad0c273e41baf2580b0cbee9c',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'javax-activation-api',
      'javax-mail-api',
    ],
  }

  jenkins::plugin { 'build-symlink':
    version       => '1.1',
    digest_string => 'dc5e517743d872ceefb86199d18c68bef4885c02c275249308a7b37ded8d4504',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'instance-identity',
      'javax-activation-api',
      'javax-mail-api',
      'sshd',
    ],
  }

  jenkins::plugin { 'build-timeout':
    version       => '1.24',
    digest_string => '4958ff53dedd0af58564bff184715cb6cc14dbe13cfff5583cdf14e7316937b4',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'instance-identity',
      'token-macro',
    ],
  }

  jenkins::plugin { 'caffeine-api':
    version       => '2.9.3-65.v6a_47d0f4d1fe',
    digest_string => '649fb9a4f730024d30b4890182e9d1c41ff388664fd81786b6cf5ddf9367d89e',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'instance-identity',
      'javax-activation-api',
      'javax-mail-api',
    ],
  }

  jenkins::plugin { 'checks-api':
    version       => '1.8.0',
    digest_string => 'f3a1b4be4108cfba5099dcb48c9e7598394a4043d4fe895ea6c83d7aeced0766',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'display-url-api',
      'instance-identity',
      'plugin-util-api',
      'workflow-step-api',
      'workflow-support',
    ],
  }

  jenkins::plugin { 'cloudbees-folder':
    version       => '6.758.vfd75d09eea_a_1',
    digest_string => '2b7556aaba895e1f48673e3b4ca3a2466764751372a4057c7a0e258e8532d16d',
    digest_type   => 'sha256',
  }

  jenkins::plugin { 'cluster-stats':
    version       => '0.4.6',
    digest_string => 'd8f725c71d845d43939ca47ca9b13136c5f1a82dc9064adcc4353ca7a771bdd2',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'bouncycastle-api',
      'command-launcher',
      'instance-identity',
      'javax-activation-api',
      'javax-mail-api',
      'jaxb',
      'jdk-tool',
      'junit',
      'matrix-project',
      'sshd',
      'trilead-api',
    ],
  }

  jenkins::plugin { 'command-launcher':
    version       => '90.v669d7ccb_7c31',
    digest_string => '38e6bf4f404d2f8264b338b773a1c930e12143f97c18bd67d6c9661427a6ada8',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'instance-identity',
      'script-security',
    ],
  }

  jenkins::plugin { 'commons-lang3-api':
    version       => '3.12.0-36.vd97de6465d5b_',
    digest_string => '98dfff9f21370d6808392fd811f90a6e173e705970309877596032be1b917ad1',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'instance-identity',
      'javax-activation-api',
      'javax-mail-api',
    ],
  }

  jenkins::plugin { 'commons-text-api':
    version       => '1.10.0-27.vb_fa_3896786a_7',
    digest_string => '88f3857141101809cc58f120e736b2dea11edb4b4fda6e6e45d86989f760337e',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'commons-lang3-api',
      'instance-identity',
      'javax-activation-api',
      'javax-mail-api',
    ],
  }

  jenkins::plugin { 'conditional-buildstep':
    version       => '1.4.2',
    digest_string => '919be166db7b7f90c1445b7dd37981e60880929362908439ba20cb25799fc98f',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'instance-identity',
      'javax-activation-api',
      'javax-mail-api',
      'run-condition',
      'token-macro',
    ],
  }

  jenkins::plugin { 'copyartifact':
    version       => '1.47',
    digest_string => '582c6f0c6d657d3b0f3a11afcfce470cdd688fd73c28896a3eb6580a844a5a5a',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'apache-httpcomponents-client-4-api',
      'instance-identity',
      'javax-activation-api',
      'javax-mail-api',
      'matrix-project',
      'structs',
    ],
  }

  jenkins::plugin { 'credentials':
    version       => '1189.vf61b_a_5e2f62e',
    digest_string => '9f6a035ddacdfb52814620089ec5e3758d4cb0d84600d13972da376a6e317d89',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'structs',
    ],
  }

  jenkins::plugin { 'credentials-binding':
    version       => '523.vd859a_4b_122e6',
    digest_string => '0a9e850728268d2750fe941ef63e35ca0eb42dfa3f425056cbd630a90d9d089a',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'credentials',
      'instance-identity',
      'javax-activation-api',
      'javax-mail-api',
      'plain-credentials',
      'ssh-credentials',
      'structs',
      'workflow-step-api',
    ],
  }

  jenkins::plugin { 'cucumber-reports':
    version       => '5.7.4',
    digest_string => 'cd6578f504324c47a5b72c5a96ab2f67a8fb8cad8892e7cc86a737b7bde7aee9',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'instance-identity',
      'javax-activation-api',
      'javax-mail-api',
      'structs',
      'token-macro',
    ],
  }

  jenkins::plugin { 'display-url-api':
    version       => '2.3.6',
    digest_string => '3f4e987912dcf1acfcc5381c4b41a0e670b6cb45dad8ed2b793893fd9b8fdfd5',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'instance-identity',
    ],
  }

  jenkins::plugin { 'echarts-api':
    version       => '5.4.0-1',
    digest_string => '9cebfa70d840c69871ff524b232331cdec4f658fb748dbc7a61fdbc06d93b286',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'bootstrap5-api',
      'font-awesome-api',
      'instance-identity',
      'jackson2-api',
      'jquery3-api',
      'plugin-util-api',
    ],
  }

  jenkins::plugin { 'email-ext':
    version       => '2.92',
    digest_string => 'd9277512f867008b302e5482f090c9f670a6a229a93e446e22cdc898e3d304a1',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'credentials',
      'instance-identity',
      'jackson2-api',
      'jakarta-mail-api',
      'junit',
      'mailer',
      'matrix-project',
      'scm-api',
      'script-security',
      'structs',
      'token-macro',
    ],
  }

  jenkins::plugin { 'envinject':
    version       => '2.881.v37c62073ff97',
    digest_string => '23a6590e1d42148149c2127b5820655947dd6b5795524cf9a05a5348df8bed9b',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'envinject-api',
      'instance-identity',
      'matrix-project',
      'script-security',
    ],
  }

  jenkins::plugin { 'envinject-api':
    version       => '1.199.v3ce31253ed13',
    digest_string => '463efd070b29d087d2db823720f6b4fab13f161d40dceeadabfbcf64e4a44c60',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'instance-identity',
    ],
  }

  jenkins::plugin { 'external-monitor-job':
    version       => '203.v683c09d993b_9',
    digest_string => 'c42bfe2a1dfa4dc5ba4a99f0940d0c926cf2b5cd93c204b058f3e27c838714cc',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'instance-identity',
      'javax-activation-api',
      'javax-mail-api',
      'sshd',
    ],
  }

  jenkins::plugin { 'font-awesome-api':
    version       => '6.2.0-3',
    digest_string => '880a26aa88d94ac4f37d7cde5191ae5cb974bd6832f8a2bf1426d0e754ea013c',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'instance-identity',
      'plugin-util-api',
    ],
  }

  jenkins::plugin { 'git':
    version       => '4.13.0',
    digest_string => '0c57763a7df5fbd350305d88e34ab3db5044646df33a2b4953b0f212a10e5b40',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'credentials',
      'credentials-binding',
      'git-client',
      'instance-identity',
      'mailer',
      'scm-api',
      'script-security',
      'ssh-credentials',
      'structs',
      'workflow-scm-step',
      'workflow-step-api',
    ],
  }

  jenkins::plugin { 'git-client':
    version       => '3.13.0',
    digest_string => 'cf887f1ee38e661f99e74ff1286a674a562705ce636456293c711dca0dd46dfd',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'apache-httpcomponents-client-4-api',
      'credentials',
      'instance-identity',
      'jsch',
      'script-security',
      'ssh-credentials',
      'structs',
      'trilead-api',
    ],
  }

  jenkins::plugin { 'global-build-stats':
    version       => '244.v27c8a_2e50a_34',
    digest_string => '465a3402f3077a849fc289fcafe8f9059772a7a20daef33d306693ee18df8724',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'cloudbees-folder',
      'instance-identity',
    ],
  }

  jenkins::plugin { 'instance-identity':
    version       => '116.vf8f487400980',
    digest_string => 'f64918f341feeb429e6c068254c52d899674d296dce9255c1cfcb4b4743631e4',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'bouncycastle-api',
    ],
  }

  jenkins::plugin { 'ionicons-api':
    version       => '31.v4757b_6987003',
    digest_string => 'ad83aa360c2c323c0c2e2303e9b86fdf8e5dd3d905e0dbe293922e04316e18ea',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'instance-identity',
    ],
  }

  jenkins::plugin { 'jackson2-api':
    version       => '2.13.4.20221013-295.v8e29ea_354141',
    digest_string => 'b75227a985c32f54cac9c947a652b45283be4279ed4ae8887d5d4c3c95c46294',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'instance-identity',
      'javax-activation-api',
      'javax-mail-api',
      'jaxb',
      'snakeyaml-api',
      'sshd',
    ],
  }

  jenkins::plugin { 'jakarta-activation-api':
    version       => '2.0.1-2',
    digest_string => '69da2e000e1ad5396a79232ba01f8b415f3f84333914a9651ebdf405d25eb20c',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'instance-identity',
    ],
  }

  jenkins::plugin { 'jakarta-mail-api':
    version       => '2.0.1-2',
    digest_string => '9749b4f012cb5c6d23e8c3835e053f7e1abc097c715b395e799f0e9cc9550c31',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'instance-identity',
      'jakarta-activation-api',
    ],
  }

  jenkins::plugin { 'javax-activation-api':
    version       => '1.2.0-5',
    digest_string => 'a7769eb7a99e4f5f008af09e6d90610c5270a19677adc26ffd98e5eeb8dddb23',
    digest_type   => 'sha256',
  }

  jenkins::plugin { 'javax-mail-api':
    version       => '1.6.2-8',
    digest_string => 'e003867d21fa94772afee1406c5d0fa06b7c0014a37e988ecefedfecb03eb1b8',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'javax-activation-api',
    ],
  }

  jenkins::plugin { 'jaxb':
    version       => '2.3.7-1',
    digest_string => '44a12a25fc976beb49f2db682db402e518b2b379c40c89e23739eb7c63df3a6c',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'instance-identity',
      'javax-activation-api',
    ],
  }

  jenkins::plugin { 'jdk-tool':
    version       => '63.v62d2fd4b_4793',
    digest_string => '3e4d0c8a3500046ff0baf3d375564a3591d3cc75a117c624ba9dbbe9296418db',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'apache-httpcomponents-client-4-api',
      'instance-identity',
    ],
  }

  jenkins::plugin { 'jquery3-api':
    version       => '3.6.1-2',
    digest_string => '13f97355ec4c3bab3da770276f827fba295a1f8bc00f38d2b0a9ccb79a61ad57',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'instance-identity',
    ],
  }

  jenkins::plugin { 'jsch':
    version       => '0.1.55.61.va_e9ee26616e7',
    digest_string => '8379691a06b084540ce6b70c11fc055720098d262b717cf46429a2afd6ca8ee6',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'instance-identity',
      'javax-activation-api',
      'javax-mail-api',
      'ssh-credentials',
      'trilead-api',
    ],
  }

  jenkins::plugin { 'junit':
    version       => '1156.vcf492e95a_a_b_0',
    digest_string => 'cda877f83c05ef82bf7e2f3995188cf1d51a2dec481077bbff3a0a118273ac20',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'bootstrap5-api',
      'checks-api',
      'display-url-api',
      'echarts-api',
      'instance-identity',
      'ionicons-api',
      'jackson2-api',
      'plugin-util-api',
      'script-security',
      'workflow-api',
      'workflow-step-api',
    ],
  }

  jenkins::plugin { 'ldap':
    version       => '2.12',
    digest_string => '07c4c432d1a7aa57850c3a0bca89f080e4ecff055540c00ae3a65ebf2bdb75a6',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'instance-identity',
      'mailer',
    ],
  }

  jenkins::plugin { 'mailer':
    version       => '438.v02c7f0a_12fa_4',
    digest_string => 'ee52de400615ea293748cf6741aef7d7181cdd5d57a588eb1a51b904e36a3159',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'display-url-api',
      'instance-identity',
      'jakarta-mail-api',
    ],
  }

  jenkins::plugin { 'matrix-auth':
    version       => '3.1.5',
    digest_string => 'c5e3b974c00a75dddf02f2ffa730c2708e837b95ec72145a33b339769392ad87',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'instance-identity',
      'javax-activation-api',
      'javax-mail-api',
      'sshd',
    ],
  }

  jenkins::plugin { 'matrix-project':
    version       => '785.v06b_7f47b_c631',
    digest_string => 'e42f01c243f2a5797649438cbf523b7a76b40d1ff3cf9075898fe1e824f2e525',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'junit',
      'script-security',
    ],
  }

  jenkins::plugin { 'mina-sshd-api-common':
    version       => '2.9.1-44.v476733c11f82',
    digest_string => 'a6dd4e001eb5d2c83fc2b9abb0563c005873d54a8542dc6a5bfc643743cf394d',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'instance-identity',
      'javax-activation-api',
      'javax-mail-api',
    ],
  }

  jenkins::plugin { 'mina-sshd-api-core':
    version       => '2.9.1-44.v476733c11f82',
    digest_string => '9a240a0e57e5ab7f35c5f02f398564ec89eb912ff11c00eacfb09c4c3b39fdcb',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'instance-identity',
      'javax-activation-api',
      'javax-mail-api',
      'mina-sshd-api-common',
    ],
  }

  jenkins::plugin { 'pam-auth':
    version       => '1.10',
    digest_string => '70a00c9b6d69b736f09c9a6ccb6a81605e91ce96d3b49ec3f78621094daf5dde',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'instance-identity',
      'javax-activation-api',
      'javax-mail-api',
    ],
  }

  jenkins::plugin { 'parameterized-trigger':
    version       => '2.45',
    digest_string => '58d1441fb5cfb4837c67d4d87a8925f45d8e99a1472a8f8010fbecc0b6ecfed9',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'instance-identity',
      'matrix-project',
      'script-security',
    ],
  }

  jenkins::plugin { 'plain-credentials':
    version       => '139.ved2b_9cf7587b',
    digest_string => 'dbbbd080cde74c6adf0b6eae9372ff1a5f4a9b18bbebba2ff4a5e509a6244c08',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'credentials',
      'instance-identity',
      'javax-activation-api',
      'javax-mail-api',
    ],
  }

  jenkins::plugin { 'plugin-util-api':
    version       => '2.18.0',
    digest_string => '8ae3041bf1537755ef6b52c130dcad8802158a96303607dc89d8fce311f03e5e',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'commons-lang3-api',
      'commons-text-api',
      'instance-identity',
    ],
  }

  jenkins::plugin { 'popper2-api':
    version       => '2.11.6-2',
    digest_string => '0fce90d92a3faa7098b87c7c62c778d23fa31fc04b9ca0f175c55e81d94ce0db',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'instance-identity',
    ],
  }

  jenkins::plugin { 'postbuildscript':
    version       => '3.2.0-460.va_fda_0fa_26720',
    digest_string => '8b65fda55c06d9e12ab294c569bc079a344aed76fbb084c6a4bd22199d76894e',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'matrix-project',
    ],
  }

  jenkins::plugin { 'PrioritySorter':
    version       => '4.1.0',
    digest_string => 'f315fb28b2d389a08a1c3fc9b9fad635aec535255843e5ee9202dab0b7ee8d33',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'instance-identity',
      'javax-activation-api',
      'javax-mail-api',
      'sshd',
    ],
  }

  jenkins::plugin { 'resource-disposer':
    version       => '0.20',
    digest_string => 'b287cdfdcc3921701fed05fbcbbb20e27ce30ec0501e0f01474e4db6d0d39fa5',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'instance-identity',
      'javax-activation-api',
      'javax-mail-api',
    ],
  }

  jenkins::plugin { 'run-condition':
    version       => '1.5',
    digest_string => '7ed94d7196676c00e45b5bf7e191831eee0e49770dced1c266b8055980b339ca',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'instance-identity',
      'javax-activation-api',
      'javax-mail-api',
      'sshd',
      'token-macro',
    ],
  }

  jenkins::plugin { 'scm-api':
    version       => '621.vda_a_b_055e58f7',
    digest_string => 'de49844880b567b71ce84153d7502a993d3bee87c948e0fad7408318bf0b61cb',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'instance-identity',
      'javax-activation-api',
      'javax-mail-api',
      'structs',
    ],
  }

  jenkins::plugin { 'scoring-load-balancer':
    version       => '1.0.1',
    digest_string => 'a7229d2945e347afb472d3c45e83ea3c4409c8710c4168912601eb46684dd3a3',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'antisamy-markup-formatter',
      'bouncycastle-api',
      'command-launcher',
      'external-monitor-job',
      'instance-identity',
      'javax-activation-api',
      'javax-mail-api',
      'jaxb',
      'jdk-tool',
      'junit',
      'ldap',
      'mailer',
      'matrix-auth',
      'matrix-project',
      'pam-auth',
      'sshd',
      'trilead-api',
      'windows-slaves',
    ],
  }

  jenkins::plugin { 'script-security':
    version       => '1189.vb_a_b_7c8fd5fde',
    digest_string => '94cd1a51f95acf45824c7396eb2aa65b43ab71f40643a2144f0abe316cec5a2f',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'caffeine-api',
      'instance-identity',
    ],
  }

  jenkins::plugin { 'snakeyaml-api':
    version       => '1.33-90.v80dcb_3814d35',
    digest_string => '5e279d36ca66ef133989012745f78ab5c19b686cbb7ddd76b444c45573bd3386',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'instance-identity',
    ],
  }

  jenkins::plugin { 'ssh-credentials':
    version       => '305.v8f4381501156',
    digest_string => '008ffb999ce9c7949c1299e1305007178bd0bedfd4c8401d6a4e92eeba635ff4',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'credentials',
      'instance-identity',
      'trilead-api',
    ],
  }

  jenkins::plugin { 'sshd':
    version       => '3.249.v2dc2ea_416e33',
    digest_string => '603bf60bb43271709932e135698c9068cd47e8c9e38a38e66344e1b2901b2dfd',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'instance-identity',
      'mina-sshd-api-core',
    ],
  }

  jenkins::plugin { 'structs':
    version       => '324.va_f5d6774f3a_d',
    digest_string => '65dd0a68c663b08e30ed254f37549e9ccfab18d27e4f1182cc7eed6d4d02c958',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'instance-identity',
      'javax-activation-api',
      'javax-mail-api',
    ],
  }

  jenkins::plugin { 'timestamper':
    version       => '1.21',
    digest_string => '85b5ee94b1435a769e3732b4302a9b61afb6913e816d8b9127d49e897500df82',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'antisamy-markup-formatter',
      'commons-lang3-api',
      'instance-identity',
      'workflow-api',
      'workflow-step-api',
    ],
  }

  jenkins::plugin { 'token-macro':
    version       => '308.v4f2b_ed62b_b_16',
    digest_string => '1523076419e72777869fb56e5b57994128de6eb5b809ce3f5410bbc5ecfe3be9',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'instance-identity',
      'javax-activation-api',
      'javax-mail-api',
      'structs',
    ],
  }

  jenkins::plugin { 'trilead-api':
    version       => '2.72.v2a_3236754f73',
    digest_string => '90545538570a6be76d2417fe5ce61e8c2fea461665390efebb19953b46900b01',
    digest_type   => 'sha256',
  }

  jenkins::plugin { 'windows-slaves':
    version       => '1.8.1',
    digest_string => '7e8554cafcfe42c8d493962a6e68c57b1bbdafc14b6c8331573bdf9b4c21fec5',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'instance-identity',
      'javax-activation-api',
      'javax-mail-api',
      'sshd',
    ],
  }

  jenkins::plugin { 'workflow-api':
    version       => '1200.v8005c684b_a_c6',
    digest_string => '28ed84f80891877cd49a8ad81475e6263dbcd6530c6f1827e3be2ac325ebf60d',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'instance-identity',
      'scm-api',
      'workflow-step-api',
    ],
  }

  jenkins::plugin { 'workflow-scm-step':
    version       => '400.v6b_89a_1317c9a_',
    digest_string => 'c0ed89da3228bfa5215b6a1724ca4a76dbbe2b939d8c4efdaa6a5a976a3145ed',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'instance-identity',
      'javax-activation-api',
      'javax-mail-api',
      'workflow-step-api',
    ],
  }

  jenkins::plugin { 'workflow-step-api':
    version       => '639.v6eca_cd8c04a_a_',
    digest_string => 'e297994ef4892b292fed850431cafe5a687fe64fbb9ddf9b7938d2b74db81763',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'instance-identity',
      'javax-activation-api',
      'javax-mail-api',
      'structs',
    ],
  }

  jenkins::plugin { 'workflow-support':
    version       => '839.v35e2736cfd5c',
    digest_string => '3fe54cab155ad9bac49d3a98df1377f5795f8acf556f829ac48b32f5567c02bd',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'caffeine-api',
      'instance-identity',
      'scm-api',
      'script-security',
      'workflow-api',
      'workflow-step-api',
    ],
  }

  jenkins::plugin { 'ws-cleanup':
    version       => '0.43',
    digest_string => 'd85329a8045a640d68b0781e40a56446e180b39c1d050d4c8079e1ce98e0f6f9',
    digest_type   => 'sha256',
    require       => Jenkins::Plugin[
      'instance-identity',
      'javax-activation-api',
      'javax-mail-api',
      'matrix-project',
      'resource-disposer',
      'structs',
    ],
  }

  # lint:endignore

}
