# Redirect from one domain to another
define tails::website::webserver::redirector (
  Boolean $ssl,
  String $target_domain,
  String $source_domain = $name,
  Enum['absent', 'present'] $ensure = 'present',
) {

  nginx::vhostsd { $source_domain:
    ensure  => $ensure,
    content => epp('tails/website/nginx/redirector.epp', {
      ssl           => $ssl,
      source_domain => $source_domain,
      target_domain => $target_domain,
    }),
    require => [
      Package[nginx],
      Openssl::Dhparam['/etc/nginx/dhparams.pem'],
    ],
  }

  if $ssl {
    tails::letsencrypt::certonly { $source_domain: }
  }

}
