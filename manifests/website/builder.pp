# Manage what's needed to build the Tails website
class tails::website::builder (
) {

  ### Sanity checks

  if $::lsbdistcodename !~ /^bullseye|sid$/ {
    fail('This class only supports Debian Bullseye and sid.')
  }

  ### Resources

  file { '/usr/local/lib/site_perl':
    ensure  => directory,
    owner   => root,
    group   => root,
    mode    => '0755',
    source  => 'puppet:///modules/tails/website/ikiwiki-libdir',
    recurse => true,
    purge   => true,
  }

  package { 'ikiwiki':
    ensure  => present,
    require => Apt::Pin['ikiwiki'],
  }

  # Fix tails#18992
  apt::source { 'ikiwiki':
    location => 'http://deb.tails.boum.org/',
    release  => 'ikiwiki',
    repos    => 'main',
  }
  apt::pin { 'ikiwiki':
    ensure   => present,
    packages => 'ikiwiki',
    origin   => 'deb.tails.boum.org',
    priority => 991,
    require  => Apt::Source['ikiwiki'],
  }

  #17005
  package { 'po4a':
    ensure  => present,
    require => $::lsbdistcodename ? { 'bookworm' => Apt::Pin['po4a'], default => undef },
  }
  if $::lsbdistcodename == 'bookworm' {
    apt::source { 'bullseye':
      ensure   => present,
      location => $tails::profile::apt::debian_url,
      release  => 'bullseye',
      repos    => $tails::profile::apt::repos,
      pin      => {
        originator => 'Debian',
        codename   => 'bullseye',
        priority   => 1,
      },
    }
    apt::pin { 'po4a':
      ensure     => present,
      packages   => 'po4a',
      originator => 'Debian',
      codename   => 'bullseye',
      priority   => 1000,
    }
  }

  $packages = [
    'libyaml-perl',
    'libyaml-libyaml-perl',
    'libyaml-syck-perl',
    'perlmagick',
  ]

  ensure_packages($packages)
}
