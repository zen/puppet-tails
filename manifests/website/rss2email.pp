# Send Tails website news to a mailing list

class tails::website::rss2email (
  Stdlib::Fqdn $public_hostname,
  String $user,
  Stdlib::Absolutepath $home_dir,
  String $email_recipient,
  String $email_sender = 'tails@boum.org',
  String $feed_name    = 'amnesia-news',
  String $feed_path    = 'news/emails.en.rss',
) {

  assert_private()

  if versioncmp($::operatingsystemmajrelease, '11') < 0 {
    fail('The tails::website::rss2email class only supports Debian Bullseye and newer.')
  }

  $bullseye_only_ensure = $::operatingsystemmajrelease ? {
    '11'    => present,
    default => absent,
  }

  $feed_url = "https://${public_hostname}/${feed_path}"
  $dirs = [
    "${home_dir}/.config",
    "${home_dir}/.local",
    "${home_dir}/.local/share",
  ]
  $config_file = "${home_dir}/.config/rss2email.cfg"
  $database = "${home_dir}/.local/share/rss2email.json"

  # Ensure we have rss2email 3.13 or newer, which can send multipart/alternative
  # i.e. HTML + plain text email (tails/tails#16723)
  apt::pin { 'rss2email':
    ensure     => $bullseye_only_ensure,
    packages   => ['rss2email', 'python3-feedparser'],
    originator => 'Debian',
    codename   => 'bookworm',
    priority   => 991,
  }

  package { 'rss2email':
    ensure  => present,
    require => Apt::Pin['rss2email'],
  }

  file { $dirs:
    ensure => directory,
    owner  => $user,
    group  => $user,
    mode   => '0700',
  }

  exec { "init r2e for ${public_hostname}":
    command => "/usr/bin/r2e add '${feed_name}' '${feed_url}' '${email_recipient}'",
    user    => $user,
    require => [
      Package[rss2email],
      File[$dirs],
    ],
    creates => $database,
  }

  $settings = {
    'url'            => $feed_url,
    'from'           => $email_sender,
    'to'             => $email_recipient,
    'name-format'    => '{feed-title}',
    'html-mail'      => 'True',
    'multipart-html' => 'True',
  }

  create_ini_settings(
    { "feed.${feed_name}" => $settings },
    {
      path    => $config_file,
      require => Exec["init r2e for ${public_hostname}"],
    }
  )

  cron { "r2e run for ${public_hostname}":
    command => '/usr/bin/r2e run',
    user    => $user,
    hour    => '*',
    minute  => '*/10',
    require => keys($settings).map |$setting| {
      Ini_setting["${config_file} [feed.${feed_name}] ${setting}"]
    },
  }

}
