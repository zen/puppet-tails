# Set up what's necessary to run automated tests
class tails::tester (
  Boolean $manage_temp_dir_mount            = false,
  String $temp_dir                          = '/tmp/TailsToaster',
  Optional[String] $temp_dir_backing_device = undef,
  String $temp_dir_fs_type                  = 'ext4',
  String $temp_dir_mount_options            = 'relatime,acl',
) {

  ### Sanity checks

  if $::lsbdistcodename !~ /^(bullseye)$/ {
    warning('The tails::tester class only supports Debian Bullseye.')
  }

  ### Resources

#  class { '::libvirt::host':
#    qemu_security_driver => 'none',
#  }

  apt::pin { 'qemu':
    packages   => ['qemu*'],
    originator => 'Debian Backports',
    codename   => 'bullseye-backports',
    priority   => 991,
  }

  apt::pin { 'libvirt':
    packages   => ['libvirt*'],
    originator => 'Debian Backports',
    codename   => 'bullseye-backports',
    priority   => 991,
  }

  apt::pin { 'obfs4proxy':
    packages   => ['obfs4proxy'],
    originator => 'Debian Backports',
    codename   => 'bullseye-backports',
    priority   => 991,
  }

  $tester_packages = [
    'cucumber',
    'devscripts',
    'dnsmasq-base',
    'ffmpeg',
    'gawk',
    'git',
    'imagemagick',
    'libcap2-bin',
    'libnss3-tools',
    'libvirt0',
    'libvirt-clients',
    'libvirt-daemon-system',
    'libvirt-dev',
    'obfs4proxy',
    'ovmf',
    'pry',
    'python3-opencv',
    'python3-pil',
    'python3-slixmpp',
    'qemu-system-x86',
    'qrencode',
    'redir',
    'ruby-guestfs',
    'ruby-json',
    'ruby-libvirt',
    'ruby-net-dns',
    'ruby-net-irc',
    'ruby-packetfu',
    'ruby-rb-inotify',
    'ruby-rjb',
    'ruby-rspec',
    'ruby-test-unit',
    'seabios',
    'tcpdump',
    'tcplay',
    'unclutter',
    'virt-viewer',
    'x11vnc',
    'x264',
    'xdotool',
    'xtightvncviewer',
    'xvfb',
  ]

  ensure_packages($tester_packages)

  # Not needed
  package { 'qemu-system-gui': ensure => absent }

  # XXX: remove once deployed everywhere
  package { 'python3-potr': ensure => absent }

  file { '/etc/tmpfiles.d/TailsToaster.conf':
    ensure  => file,
    owner   => root,
    group   => root,
    mode    => '0644',
    content => "D  ${temp_dir}  0755  root  root  -\n",
  }

  if $manage_temp_dir_mount {
    validate_string(
      $temp_dir_backing_device,
      $temp_dir_fs_type,
      $temp_dir_mount_options
    )

    mount { $temp_dir:
      ensure  => mounted,
      device  => $temp_dir_backing_device,
      fstype  => $temp_dir_fs_type,
      options => $temp_dir_mount_options,
      require => File['/etc/tmpfiles.d/TailsToaster.conf']
    }

  }
}
