# Manage various email reminders

class tails::meeting::reminders {

  include tails::meeting

  tails::meeting::reminder { 'FT_meeting':
    ensure                 => absent,  # disabled until FT decides new meeting schedule
    addresses              => ['tails-foundations@boum.org'],
    subject                => 'Tails Foundations Team meeting: ',
    template               => 'FT_meeting_template.eml',
    day_of_month           => 3,
    reminders              => [10],
    skip_friday_to_sunday  => true,
    append_date_to_subject => true,
  }

  tails::meeting::reminder { 'Code of Conduct':
    addresses       => [
      'tails-dev@boum.org',
      'tails-l10n@boum.org',
      'tails-testers@boum.org',
    ],
    subject         => 'Reminder about our Code of Conduct',
    template        => 'code-of-conduct.eml',
    append_web_page => 'https://gitlab.tails.boum.org/tails/tails/-/raw/master/wiki/src/contribute/working_together/code_of_conduct.mdwn',
    day_of_month    => 2,
    months          => [3, 6, 9, 12],
    reminders       => [1],
  }

  tails::meeting::reminder { 'Monthly report':
    addresses    => ['tails-dev@boum.org'],
    subject      => 'Preparing the next monthly report',
    template     => 'monthly-report.eml',
    day_of_month => 2,
    reminders    => [1],
  }

  tails::meeting::reminder { 'Reimbursements':
    addresses    => ['tails-dev@boum.org'],
    subject      => 'Reminder about reimbursements and sponsorship to attend events on behalf of Tails',
    template     => 'reimbursements.eml',
    day_of_month => 2,
    months       => [1, 4, 7, 10],
    reminders    => [1],
  }

}
