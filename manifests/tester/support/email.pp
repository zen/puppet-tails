# Manage services needed to provide a local email server that can be used
# by Thunderbird in our automated test suite.

class tails::tester::support::email (
  String $email_password      = 'secret',
  String $email_password_salt = $::fqdn,
  String $email_user          = "test@${::fqdn}",
) {

  ### Common resources

  $packages = [
    'dovecot-core',
    'dovecot-imapd',
    'dovecot-lmtpd',
    'dovecot-pop3d',
    'ssl-cert',
    'swaks',
  ]

  ensure_packages($packages)

  $hashed_email_password = pw_hash(
    $email_password, 'SHA-512', $email_password_salt
  )

  ### Constants

  $dovecot_ssl_cert      = '/etc/dovecot/private/dovecot.pem'
  $dovecot_ssl_key       = '/etc/dovecot/private/dovecot.key'

  ### Dovecot

  service { 'dovecot':
    ensure  => running,
    enable  => true,
    require => [
      File['/etc/dovecot/conf.d/99-tails-tester-support.conf'],
      File_line['dovecot_disable_auth-system'],
      Package['dovecot-imapd'],
      Package['dovecot-pop3d'],
      Package['ssl-cert'],
      User['vmail'],
    ],
  }

  user { 'vmail':
    uid        => '5000',
    home       => '/var/vmail',
    managehome => true,
  }

  group { 'vmail':
    gid        => '5000',
  }

  file { $dovecot_ssl_cert:
    ensure  => link,
    target  => '/etc/ssl/certs/ssl-cert-snakeoil.pem',
    require => Package['ssl-cert'],
  }

  file { $dovecot_ssl_key:
    ensure  => link,
    target  => '/etc/ssl/private/ssl-cert-snakeoil.key',
    require => Package['ssl-cert'],
  }

  file { '/etc/dovecot/conf.d/99-tails-tester-support.conf':
    content => template('tails/tester/support/email/dovecot/tails-tester-support.conf.erb'),
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    require => [
      File[$dovecot_ssl_cert],
      File[$dovecot_ssl_key],
      Package['dovecot-core'],
    ],
    notify  => Service['dovecot'],
  }

  file { '/etc/dovecot/passwd':
    content => "${email_user}:${hashed_email_password}::::::\n",
    owner   => 'root',
    group   => 'dovecot',
    mode    => '0640',
  }

  file_line { 'dovecot_disable_auth-system':
    path   => '/etc/dovecot/conf.d/10-auth.conf',
    line   => '#!include auth-system.conf.ext',
    match  => '#?!include auth\-system\.conf\.ext',
    notify => Service['dovecot'],
  }

  ### nginx

  class { '::nginx':
    access_log => 'noip',
    require    => Openssl::Dhparam['/etc/nginx/dhparams.pem'],
  }
  openssl::dhparam { '/etc/nginx/dhparams.pem':
    ensure => present,
    size   => 4096,
  }

  file { '/etc/nginx/sites-enabled/default':
    ensure  => absent,
    require => Package[nginx],
  }

  nginx::vhostsd { $::fqdn:
    content => template('tails/tester/support/email/nginx/vhost.erb'),
    notify  => Service[nginx],
    require => Package[nginx],
  }

  file { [
    '/var/www/html/.well-known',
    '/var/www/html/.well-known/autoconfig',
    '/var/www/html/.well-known/autoconfig/mail',
  ]:
    ensure  => directory,
    owner   => 'root',
    group   => 'www-data',
    mode    => '0750',
    require => Package['nginx'],
  }

  file { '/var/www/html/.well-known/autoconfig/mail/config-v1.1.xml':
    owner   => 'root',
    group   => 'www-data',
    mode    => '0640',
    content => template('tails/tester/support/email/nginx/autoconfig.xml.erb')
  }

}
