# Manage a cronjob that runs check-mirrors.rb
define tails::check_mirrors::cronjob (
  Integer[0, 23] $hour,
  Integer[0, 59] $minute,
  Array[String] $args = [],
) {

  assert_private()

  $args_str = join($args, ' ')

  $flock_timeout = 30
  $error_msg = "Cron run of `check-mirrors.rb` timed out waiting for lock after ${flock_timeout}s."

  cron { "tails_check_mirrors ${title}":
    command     => "sleep \$(( \$( </dev/urandom od -N2 -t u2 -A none ) >> 5 )) && flock --wait ${flock_timeout} check-mirrors.lock ${tails::check_mirrors::cronjob_script} ${args_str} || echo '${error_msg}' >&2", # lint:ignore:140chars -- command
    user        => $tails::check_mirrors::user,
    hour        => $hour,
    minute      => $minute,
    require     => [
      Vcsrepo[$tails::check_mirrors::repo_checkout],
      Package[$tails::check_mirrors::needed_packages],
      Mailalias[$tails::check_mirrors::user],
      Exec['Import Tails signing key'],
      File[$tails::check_mirrors::cronjob_script],
    ],
    # By default, check-mirrors.rb would create its per-run temporary
    # directory in $CWD, i.e. in this context $HOME, which is never
    # cleaned up automatically; so interrupted runs would leave
    # temporary files behind them forever. Let's ask check-mirrors.rb
    # to instead create its per-run temporary directory in a place
    # that is cleaned up on boot. This requires ~2.5 GiB free space
    # in /tmp so if we ever need to include this class on a system
    # that has a smaller tmpfs mounted there, we'll need to make
    # this configurable.
    environment => [ 'TMPDIR=/tmp' ],
  }

}
