# Setup the webserver to serve the translation platform
class tails::weblate::webserver inherits tails::weblate::params {

  ensure_packages([
    'apache2',
    'modsecurity-crs',
    'libapache2-mod-security2',
  ])

  service { 'apache2':
    ensure  => running,
    require => Package[apache2],
  }

  # FIXME: remove once deployed
  exec { '/usr/sbin/a2dismod rewrite':
    onlyif  => '/usr/bin/test -f /etc/apache2/mods-enabled/rewrite.load',
    require => Package['apache2'],
    notify  => Service['apache2'],
  }

  exec { '/usr/sbin/a2enmod proxy':
    creates => '/etc/apache2/mods-enabled/proxy.load',
    notify  => Service['apache2'],
    require => Package['apache2'],
  }

  exec { '/usr/sbin/a2enmod headers':
    creates => '/etc/apache2/mods-enabled/headers.load',
    notify  => Service['apache2'],
    require => Package['apache2'],
  }

  exec { '/usr/sbin/a2enmod proxy_http':
    creates => '/etc/apache2/mods-enabled/proxy_http.load',
    notify  => Service['apache2'],
    require => [
      Package['apache2'],
      Exec['/usr/sbin/a2enmod proxy'],
    ]
  }

  file { $weblate_www_dir:
    ensure => directory,
    mode   => '0755',
    owner  => $system_uid,
    group  => $system_gid,
  }

  # By blocking crawlers from accesing `/translate`, we avoid them from wasting
  # resources, triggering errors in Weblate and possibly indexing unreviewed
  # content.
  file { "${weblate_www_dir}/robots.txt":
    ensure  => file,
    owner   => $system_uid,
    group   => $system_gid,
    mode    => '0644',
    content => "User-agent: *\nDisallow: /translate\n",
  }

  file { "${weblate_config_dir}/apache-vhost.conf":
    ensure  => present,
    content => epp('tails/weblate/apache-vhost.conf.epp', {
      staging_www_dir => $staging_www_dir,
      weblate_www_dir => $weblate_www_dir,
    }),
    owner   => root,
    group   => $system_gid,
    mode    => '0664',
    notify  => Service[apache2],
    require => [
      File[$weblate_config_dir],
      File[$weblate_www_dir],
    ]
  }

  file { '/etc/apache2/sites-available/000-default.conf':
    ensure  => symlink,
    target  => "${weblate_config_dir}/apache-vhost.conf",
    require => Package[apache2],
    notify  => Service[apache2],
  }

  file { '/etc/modsecurity/modsecurity.conf':
    ensure  => present,
    source  => 'puppet:///modules/tails/weblate/modsecurity/modsecurity.conf',
    owner   => root,
    group   => root,
    mode    => '0644',
    require => Package[modsecurity-crs],
    notify  => Service[apache2],
  }

  file { '/etc/modsecurity/crs/crs-setup.conf':
    ensure  => present,
    source  => 'puppet:///modules/tails/weblate/modsecurity/crs-setup.conf',
    owner   => root,
    group   => root,
    mode    => '0644',
    require => Package[modsecurity-crs],
    notify  => Service[apache2],
  }

}
