# A copy of the Tails website to preview translations
class tails::weblate::staging_website(
) inherits tails::weblate::params {

  include tails::website::builder

  file { $staging_www_dir:
    ensure => directory,
    mode   => '0755',
    owner  => $system_uid,
    group  => $system_gid,
  }

  file { "${weblate_config_dir}/ikiwiki.setup":
    content => epp('tails/website/ikiwiki.setup.epp', {
      src_dir                      => "${weblate_repos_dir}/staging/wiki/src",
      web_dir                      => $staging_www_dir,
      url                          => "https://${staging_site_domain}",
      with_cgi                     => false,
      cgi_url                      => undef,
      is_staging                   => true,
      weblate_additional_languages => $weblate_additional_languages,
      po_slave_languages           => $weblate_slave_languages,
      underlays                    => [],
      git_dir                      => '',
    }),
    mode    => '0644',
    owner   => weblate,
    group   => weblate,
  }

}
