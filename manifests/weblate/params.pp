# Parameters that are common to many weblate-related classes.
class tails::weblate::params (
  String $system_user                       = 'weblate',
  String $system_group                      = 'weblate',
  Integer $system_uid                       = 2000000,
  Integer $system_gid                       = 2000000,
  Integer $ns_weblate_uid                   = $system_uid + 1000,  # Namespace UID/GID to use inside the Weblate container,
  Integer $ns_root_gid                      = $system_gid,         # see `tails::weblate::podman` for more info about this.
  Stdlib::Absolutepath $system_logs_dir     = '/var/log/weblate',
  Stdlib::Absolutepath $weblate_home        = '/var/lib/weblate',
  Stdlib::Absolutepath $weblate_scripts_dir = "${weblate_home}/scripts",
  Stdlib::Absolutepath $weblate_config_dir  = "${weblate_home}/config",
  Stdlib::Absolutepath $weblate_data_dir    = "${weblate_home}/data",
  Stdlib::Absolutepath $weblate_repos_dir   = "${weblate_home}/repositories",
  Stdlib::Absolutepath $weblate_logs_dir    = "${weblate_home}/logs",
  Stdlib::Absolutepath $weblate_www_dir     = '/var/www/weblate',
  Stdlib::Absolutepath $staging_www_dir     = '/var/www/staging',
  Stdlib::Absolutepath $tmserver_data_dir   = '/var/lib/tmserver',
  String $postgres_user                     = 'weblate',
  String $postgres_database                 = 'weblate',
  String $weblate_version                   = '4.9.1-1',
  Stdlib::Fqdn $weblate_site_domain         = 'translate.tails.boum.org',
  Stdlib::Fqdn $staging_site_domain         = 'staging.tails.boum.org',
) inherits tails::website::params {

}
