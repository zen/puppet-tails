# Manage a nginx vhost that reverse proxies to a weblate instance.
class tails::weblate::reverse_proxy (
  Stdlib::Fqdn $upstream_hostname   = 'translate.lizard',
  Stdlib::Port $upstream_port       = 80,
  ) inherits tails::weblate::params {

  $nginx_includename = 'tails_weblate_reverse_proxy'
  $nginx_nophp = true
  $upstream_address = "${upstream_hostname}:${upstream_port}"
  $public_hostname = $weblate_site_domain

  nginx::vhostsd { $weblate_site_domain:
    content => template('tails/nginx/reverse_proxy.vhost.erb'),
    require => [
      Package[nginx],
      File['/etc/nginx/include.d/site_ssl.conf'],
      Nginx::Included[$nginx_includename],
      Tails::Letsencrypt::Certonly[$weblate_site_domain],
    ];
  }

  nginx::included { $nginx_includename:
    content => template('tails/nginx/reverse_proxy.inc.erb'),
  }

  tails::letsencrypt::certonly { $weblate_site_domain: }

}
