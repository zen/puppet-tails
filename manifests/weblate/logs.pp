# Manage log directories and rotation
class tails::weblate::logs inherits tails::weblate::params {

  file { $system_logs_dir:
    ensure => directory,
    owner  => root,
    group  => $system_gid,
    mode   => '2770',
  }

  file { $weblate_logs_dir:
    ensure => link,
    target => $system_logs_dir,
  }

  file { '/etc/logrotate.d/weblate':
    source  => 'puppet:///modules/tails/weblate/logrotate/logrotate.conf',
    owner   => root,
    group   => root,
    mode    => '0644',
    require => File[$system_logs_dir],
  }

}

