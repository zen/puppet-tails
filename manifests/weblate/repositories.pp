# Clone and checkout the needed Git repositories
class tails::weblate::repositories inherits tails::weblate::params {

  file { $weblate_repos_dir:
    ensure => directory,
    owner  => $system_uid,
    group  => $system_gid,
    mode   => '0755',
  }


  ### Weblate repository

  $vcs_root = "${weblate_data_dir}/vcs"
  $weblate_repo = "${vcs_root}/tails/index"

  vcsrepo { $weblate_repo:
    ensure   => present,
    provider => git,
    remote   => 'origin',
    source   => {
      # used by Weblate
      'origin' => 'https://gitlab.tails.boum.org/tails/tails.git',
      # update_weblate_components.py, that operates on this working copy,
      # needs to keep track of the status of the remote repo independently
      # of Weblate
      'cron'   => 'https://gitlab.tails.boum.org/tails/tails.git',
    },
    branch   => 'master',  # This only has an effect when cloning.
  }

  exec { 'Ensure Weblate repository UID/GID for containerized Weblate':
    command => "/bin/chown -R ${ns_weblate_uid}:${ns_root_gid} ${vcs_root}",
    onlyif  => "/usr/bin/find ${vcs_root} -not \\( -uid ${ns_weblate_uid} -a -gid ${ns_root_gid} \\) | /bin/grep .",
    require => [
      Vcsrepo[$weblate_repo],
      Package['grep'],
    ],
  }

  exec { 'Ensure Weblate repository dir is setgid':
    command => "/usr/bin/find ${vcs_root} -type d -exec chmod 2755 '{}' \\;",
    onlyif  => "/usr/bin/find ${vcs_root} -type d -not -perm 2755 | /bin/grep .",
    require => [
      Vcsrepo[$weblate_repo],
      Package['grep'],
    ],
  }

  # Some pieces of the Translation Platform run in the host and must be able to
  # read the contents of the Weblate repo (eg. updating tmserver database).
  exec { 'Ensure Weblate repository files are readable by the host\'s `weblate` group':
    command => "/usr/bin/find ${vcs_root} -not -perm -g=r -exec chmod g+r '{}' \\;",
    onlyif  => "/usr/bin/find ${vcs_root} -not -perm -g=r | /bin/grep .",
    require => [
      Vcsrepo[$weblate_repo],
      Package['grep'],
    ],
  }


  ### Integration repository

  $integration_repo = "${weblate_repos_dir}/integration"
  $integration_sources = {
    'origin'             => 'https://gitlab.tails.boum.org/tails/tails.git',
    'weblate'            => '/app/data/vcs/tails/index/.git',
    # Note that we currently don't manage the generation or configuration of
    # the SSH key needed to push to the gatekeeer repository. This has to be
    # done by hand.
    'weblate-gatekeeper' => 'gitolite@puppet-git.lizard:weblate-gatekeeper.git',
  }

  vcsrepo { $integration_repo:
    ensure   => present,
    provider => git,
    remote   => 'origin',
    source   => $integration_sources,
    require  => Vcsrepo[$weblate_repo],
    branch   => 'master',  # This only has an effect when cloning.
  }

  exec { 'Ensure Integration repository UID/GID':
    command => "/bin/chown -R ${ns_weblate_uid}:${ns_root_gid} ${integration_repo}",
    onlyif  => "/usr/bin/find ${integration_repo} -not \\( -uid ${ns_weblate_uid} -a -gid ${ns_root_gid} \\) | /bin/grep .",
    require => [
      Package['grep'],
      Vcsrepo[$integration_repo],
    ],
  }


  ### Staging website repository

  $staging_repo = "${weblate_repos_dir}/staging"

  vcsrepo { $staging_repo:
    ensure   => present,
    provider => git,
    remote   => 'origin',
    source   => 'https://gitlab.tails.boum.org/tails/tails.git',

    # The website is built from the `master` branch of the Tails Git repo, so we
    # pass it here. Passing a branch only affects cloning and will not change
    # the checked out branch, so this should not interfere with any branch
    # operations made on the repository (by integration scripts, for example).
    branch   => 'master',
    require  => Vcsrepo[$weblate_repo],
  }

  exec { 'Ensure staging repository UID/GID for containerized Weblate':
    command => "/bin/chown -R ${ns_weblate_uid}:${ns_root_gid} ${staging_repo}",
    onlyif  => "/usr/bin/find ${staging_repo} -not \\( -uid ${ns_weblate_uid} -a -gid ${ns_root_gid} \\) | /bin/grep .",
    require => [
      Vcsrepo[$staging_repo],
      Package['grep'],
    ]
  }

  # Files in the staging repository must be writable by the weblate system user
  # outside of the container
  exec { 'Ensure staging repository is writable by the host\'s `weblate` group':
    command => "/usr/bin/find ${staging_repo} -exec chmod g+w '{}' \\;",
    onlyif  => "/usr/bin/find ${staging_repo} -not -perm -g=w | /bin/grep .",
    require => [
      Vcsrepo[$staging_repo],
      Package['grep'],
    ],
  }

  ### Ensure Git configuration on repositories

  $git_checkouts = [
    $weblate_repo,
    $integration_repo,
    $staging_repo,
  ]

  $git_checkouts.each |String $git_checkout| {

    $git_config = "${git_checkout}/.git/config"

    file { $git_config:
      owner   => $ns_weblate_uid,
      group   => $ns_root_gid,
      mode    => '0664',
      require => Vcsrepo[$git_checkout],
    }

    create_ini_settings(
      {
        'user' => {
          'name'  => 'Tails translators',
          'email' => 'tails-l10n@boum.org',
        },
      },
      {
        path    => $git_config,
        require => File[$git_config],
      }
    )
  }

}
