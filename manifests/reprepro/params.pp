# Default parameters that are shared between tails::reprepro::* classes.
class tails::reprepro::params () {

  $snapshots_architectures = {
    'bullseye'     => ['amd64', 'source'],
    'sid'          => ['amd64', 'source'],
    'experimental' => ['amd64', 'source'],
    'tails'        => ['amd64', 'source'],
    'torproject'   => ['amd64'],
  }

  $signing_key = 'D68F87149EBA77541573C1C12453AA9CE4123A9A'

  $snapshots_signwith = $signing_key

}
