# Manage GnuPG secret keys used by reprepro snapshot classes
define tails::reprepro::snapshots::secret_keys (
  Stdlib::Absolutepath $basedir,
  Stdlib::Absolutepath $homedir,
  String $user,
) {

  ### Resources

  $keys_src_file = "${homedir}/private-keys.asc"

  exec { "tails-reprepro-import-private-keys_${name}":
    user        => $user,
    group       => $user,
    command     => "gpg --batch --quiet --import '${keys_src_file}'",
    subscribe   => File[$keys_src_file],
    refreshonly => true,
  }

  # Make sure the imported keys are up-to-date (works around the fact
  # that if one uses this define multiple times on the same system,
  # then only one of the corresponding Exec's is run, due to refreshonly)
  cron { "tails-reprepro-import-private-keys_${name}":
    user    => $user,
    minute  => 47,
    command => "gpg --batch --quiet --import '${keys_src_file}' >/dev/null 2>&1 || true",
  }

}
