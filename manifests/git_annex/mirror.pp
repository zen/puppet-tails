# Manage a mirror of a git-annex repository
define tails::git_annex::mirror (
  Pattern[/\A[a-z_-]+\z/] $user,
  Stdlib::Absolutepath $home,
  String $ssh_keyname,

  Stdlib::Absolutepath $checkout_dir,
  String $remote_repo,

  Enum['present', 'absent'] $ensure           = 'present',
  String $mode                                = 'mirror',
  Boolean $direct_mode                        = true,
  Array[String] $pull_hour                    = ['*'],
  Array[String] $pull_minute                  = ['14', '29', '44', '59'],

  Boolean $manage_mount                       = false,
  $mount_point                                = false,
  $mount_device                               = false,
  $mount_fstype                               = false,
  $mount_options                              = false,

  String $webserver                           = 'nginx',
  Boolean $manage_vhost                       = false,
  $vhost_template                             = false,
  $vhost_auth_source                          = false,
) {

  ### Resources

  vcsrepo { $checkout_dir:
    ensure   => $ensure,
    provider => git,
    source   => $remote_repo,
    user     => $user,
    require  => Sshkeys::Set_client_key_pair[$ssh_keyname],
  }

  file { $checkout_dir:
    ensure  => directory,
    owner   => $user,
    group   => 'www-data',
    mode    => '0750',
    require => Vcsrepo[$checkout_dir],
  }

  exec { "Switch ${name} to direct mode":
    command => 'git annex direct',
    user    => $user,
    cwd     => $checkout_dir,
    creates => "${checkout_dir}/.git/annex",
    require => [
      Package['git-annex'],
      Vcsrepo[$checkout_dir],
    ],
  }

  $service_ensure = $ensure ? {
    absent  => stopped,
    default => running,
  }
  $daemon_reload_exec = "systemctl-daemon-reload-git-annex-mirror-${name}"
  exec { $daemon_reload_exec:
    refreshonly => true,
    command     => '/bin/systemctl daemon-reload',
  }
  $timer = "git-annex-mirror-${name}.timer"
  file { "/etc/systemd/system/${timer}":
    ensure  => $ensure,
    content => template('tails/git_annex/mirror.timer.erb'),
    notify  => [
      Exec[$daemon_reload_exec],
      Service[$timer],
    ],
  }
  $command = "/usr/local/bin/pull-git-annex ${mode}"
  file { "/etc/systemd/system/git-annex-mirror-${name}.service":
    content => template('tails/git_annex/mirror.service.erb'),
    notify  => Exec[$daemon_reload_exec],
  }
  service { $timer:
    ensure   => $service_ensure,
    provider => 'systemd',
    enable   => true,
    require  => [
      Exec["Switch ${name} to direct mode"],
      File['/usr/local/bin/pull-git-annex'],
      Exec[$daemon_reload_exec],
    ],
  }

  user { $user:
    ensure => $ensure,
    system => true,
    home   => $home,
  }

  file { [ $home, "${home}/.ssh" ]:
    ensure => directory,
    owner  => $user,
    group  => $user,
    mode   => '0700',
  }

  sshkeys::set_client_key_pair { $ssh_keyname:
    keyname => $ssh_keyname,
    user    => $user,
    home    => $home,
    require => [
      User[$user],
      File["${home}/.ssh"],
    ],
  }

  if $manage_mount {
    validate_string($mount_point)
    validate_string($mount_device)
    validate_string($mount_fstype)
    validate_string($mount_options)

    file { $mount_point:
      ensure => directory,
      owner  => $user,
      group  => 'www-data',
      mode   => '2755',
    }

    mount { $mount_point:
      ensure  => mounted,
      device  => $mount_device,
      fstype  => $mount_fstype,
      options => $mount_options,
    }

  }

  if $manage_vhost {
    case $webserver {

      'nginx': {
        ensure_packages(['libnginx-mod-http-fancyindex'])

        nginx::vhostsd { $name:
          content => template($vhost_template),
          require => Package['libnginx-mod-http-fancyindex'],
        }
        if $vhost_auth_source {
          nginx::authd { $name:
            source  => $vhost_auth_source,
          }
        }
      }

      default: {
        fail("Unsupported webserver ${webserver}")
      }

    }
  }

}
