# Make a system ready to build Tails IUKs.
# Design doc: https://tails.boum.org/contribute/design/incremental_upgrades/
class tails::iuk_builder {

  $iuk_builder_packages = [
    'attr',
    'libarchive-tar-wrapper-perl',
    'libarchive-tools',
    'libcarp-assert-more-perl',
    'libcarp-assert-perl',
    'libclass-xsaccessor-perl',
    'libdevice-cdio-perl',
    'libdpkg-perl',
    'libfile-which-perl',
    'libfilesys-df-perl',
    'libfunction-parameters-perl',
    'libgnupg-interface-perl',
    'libipc-system-simple-perl',
    'libmoo-perl',
    'libmoox-handlesvia-perl',
    'libmoox-late-perl',
    'libmoox-options-perl',
    'libnamespace-clean-perl',
    'libpath-tiny-perl',
    'libstring-errf-perl',
    'libtry-tiny-perl',
    'libtypes-path-tiny-perl',
    'libyaml-libyaml-perl',
    'libyaml-perl',
    'nocache',
    'rsync',
    'squashfs-tools-ng',
  ]

  ensure_packages($iuk_builder_packages)

  file { '/usr/local/bin/wrap_tails_create_iuks':
    ensure => present,
    source => 'puppet:///modules/tails/jenkins/slaves/isobuilders/wrap_tails_create_iuks',
    mode   => '0755',
    owner  => 'root',
    group  => 'root',
  }

}
