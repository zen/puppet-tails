# profile for our monitoring master
class tails::profile::monitormaster (
  String $spamaddress = lookup(tails::profile::sysadmins::email,undef,undef,'sysadmins@example.org'),
) {

  $plugin_packages = [
    'monitoring-plugins',
    'monitoring-plugins-basic',
    'monitoring-plugins-common',
    'monitoring-plugins-standard',
    'nagios-plugins-contrib',
    'python3-nagiosplugin',
  ]
  ensure_packages($plugin_packages, {ensure => latest} )

  class { 'monitoring::master':
    spamaddress => $spamaddress,
  }

  tor::daemon::onion_service { "icinga-api-${::fqdn}":
    ports   => [ '5665' ],
  }

  include backupninja
  backupninja::mysql { 'backup-monitoring-db':
    ensure => present,
  }

  tirewall::accept_trusted_subnets { 'Icinga2 Master':
    dport => 5665,
  }

}
