# profile for release tasks

class tails::profile::release_misc (
) {
  rbac::user { 'ft-commit': }
  include ::tails::git_annex
}
