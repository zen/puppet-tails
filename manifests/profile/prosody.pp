# Profile for XMPP server

class tails::profile::prosody (
  Stdlib::Absolutepath $prosody_config_dir = '/etc/prosody',
) {

  ### Sanity checks

  if $::operatingsystem != 'Debian' or versioncmp($::operatingsystemmajrelease, '11') < 0 {
    fail('This module only supports Debian 11 or newer.')
  }

  ### RBAC

  rbac::user { 'im-admin': }

  user { 'prosody_admin':
    ensure => present,
    system => true,
    gid    => 'prosody_admin',
  }

  group { 'prosody_admin':
    ensure => present,
    system => true,
  }

  rbac::group { 'im-admin-prosody':
    group => 'prosody',
    role  => 'im-admin',
    require   => Package['prosody'],
  }

  rbac::group { 'im-admin-prosody_admin':
    group => 'prosody_admin',
    role  => 'im-admin',
    require   => Group['prosody_admin'],
  }

  sudo::conf { 'prosody-admin':
    content => 'Cmnd_Alias PROSODY_ADMIN_CMDS =                \
  /bin/less /var/log/prosody/prosody.log,      \
  /bin/less /var/log/prosody/prosody.err,      \
  /bin/systemctl force-reload prosody.service, \
  /bin/systemctl reload prosody.service,       \
  /bin/systemctl restart prosody.service,      \
  /bin/systemctl start prosody.service,        \
  /bin/systemctl stop prosody.service,         \
  /usr/bin/prosodyctl

%prosody_admin ALL = (root) NOPASSWD: PROSODY_ADMIN_CMDS
%prosody_admin ALL = (prosody) NOPASSWD: ALL
%prosody_admin ALL = (prosody_admin) NOPASSWD: ALL',
    require => Group['prosody_admin'],
  }

  ### Onion service

  tor::daemon::onion_service { 'xmpp-hidden-v3':
    ports   => [ '5222' ],
  }

  ### Packages

  ensure_packages([
    'prosody', 'ssl-cert', 'ca-certificates', 'lua-dbi-sqlite3'
  ])

  $config_dirs = [
    $prosody_config_dir,
    "${prosody_config_dir}/certs",
    "${prosody_config_dir}/conf.avail",
    "${prosody_config_dir}/conf.d",
  ]

  tails::dpkg_statoverride { $config_dirs:
    user  => 'prosody_admin',
    group => 'prosody',
    mode  => '2750',
  }

  $config_files = [
    "${prosody_config_dir}/prosody.cfg.lua",
    "${prosody_config_dir}/migrator.cfg.lua",
  ]

  tails::dpkg_statoverride { $config_files:
    user  => 'prosody_admin',
    group => 'prosody',
    mode  => '640',
  }

}
