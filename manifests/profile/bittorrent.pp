# profile for bittorrent server

class tails::profile::bittorrent (
) {

  package { ['transmission-cli', 'transmission-daemon']:
    ensure => present
  }

  rbac::user { 'ft': }

  rbac::group { 'ft-to-debian-transmission':
    group   => 'debian-transmission',
    role    => 'ft',
    require => Package['transmission-daemon'],
  }

  tirewall::public_service { 'tails::profile::bittorrent (TCP)':
    proto => 'tcp',
    dport => 51413,
  }

  tirewall::public_service { 'tails::profile::bittorrent (UDP)':
    proto => 'udp',
    dport => 51413,
  }

}
