# Class to create sysadmin users

class tails::profile::sysadmins (
  String $email = 'sysadmins@example.org',
) {

# manage root, n.b. this is also needed to use
# sshkeys::set_client_key_pair with parameter user => 'root'.

  user { 'root': ensure => present }
  file { '/root':
    ensure => directory,
    owner  => root,
    group  => staff,
    mode   => '0750',
  }
  mailalias { 'root':
    recipient => $email,
  }

# create sysadmin users

  rbac::user { 'sysadmin': }

# grant ssh access to root

  rbac::ssh { 'sysadmin-to-root':
    user => 'root',
    role => 'sysadmin',
  }

# ensure proper group membership

  $groups = [ 'adm', 'src', 'staff', 'sudo' ]

  $groups.each | String $group | {
    rbac::group { "sysadmin-${group}":
      group => $group,
      role  => 'sysadmin',
    }
  }

# intrigeri-specific preferences

#  include intrigeri::home
#  include intrigeri::packages::base
#  include intrigeri::packages::emacs

# zen-specific preferences

  ensure_packages(['lynx'])
  ensure_packages(['fish'], { require => Package['lynx'] })

  file { '/home/zen/.hushlogin':
    ensure  => file,
    mode    => '0644',
    owner   => 'zen',
    group   => 'zen',
    require => User['zen'],
  }

  file { ['/home/zen/.config', '/home/zen/.config/fish']:
    ensure  => directory,
    mode    => '0755',
    owner   => 'zen',
    group   => 'zen',
    require => User['zen'],
  }

  file { '/home/zen/.config/fish/config.fish':
    ensure  => file,
    mode    => '0644',
    owner   => 'zen',
    group   => 'zen',
    require => File['/home/zen/.config/fish'],
  }

  file_line { 'fish_no_greeting_zen':
    ensure  => present,
    path    => '/home/zen/.config/fish/config.fish',
    line    => 'set fish_greeting',
    match   => '^set fish_greeting',
    replace => true,
    require => [
      File['/home/zen/.config/fish/config.fish'],
      Package['fish'],
    ],
  }

}
