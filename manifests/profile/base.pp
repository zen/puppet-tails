# Base class for team-admin'd systems

class tails::profile::base {

  #
  # Sanity checks
  #

  if $::operatingsystem != 'Debian' {
    fail('This module only supports Debian.')
  }

  #
  # Include profiles we want on all nodes
  #

  include tails::profile::augeas
  include tails::profile::ssh
  include tails::profile::packages
  include tails::profile::systemd
  include tails::profile::sudo
  include tails::profile::fstrim
  include tails::profile::cron
  include tails::profile::timezone
  include tails::profile::tor
  include tails::profile::loginrecords
  include tails::profile::vim
  include tails::profile::fsck
  include tails::profile::sysctl
  include tails::profile::hosts

  if $::virtual == 'physical' {
    include tails::profile::physical
  } else {
    include tails::profile::virtual
  }

}
