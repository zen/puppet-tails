# @summary
#   Manage a system's timezone.
#
# @example
#   include tails::profile::timezone
#
# @param region
#   A string defining the system's region, defaults to Etc.
#
# @param locality
#   A string defining the system's locality, defaults to UTC.
#
class tails::profile::timezone (
  String $region   = 'Etc',
  String $locality = 'UTC',
) {
  class { 'timezone':
    region   => $region,
    locality => $locality,
  }
}
