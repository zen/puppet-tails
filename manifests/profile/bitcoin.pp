# profile for bitcoin server

class tails::profile::bitcoin (
  String $mailto = lookup(tails::profile::sysadmins::email,undef,undef,'sysadmins@example.org'),
) {

  # install bitcoind

  apt::pin { 'bitcoind':
    packages   => 'bitcoind',
    originator => 'Debian',
    codename   => 'sid',
    priority   => 990,
  }

  include ::bitcoind

  # grant accountants access

  rbac::ssh { 'accounting-to-bitcoin':
    user    => 'bitcoin',
    role    => 'accounting',
    require => Class['bitcoind'],
  }

  # arrange backups

  file { '/var/backups/bitcoin':
    ensure => directory,
    owner  => bitcoin,
    group  => bitcoin,
    mode   => '0700',
  }

  $wallet_backup = '/var/backups/bitcoin/wallet.dat'
  cron { 'backup-bitcoin-wallet':
    command => "bitcoin-cli backupwallet \"${wallet_backup}.new\" && mv \"${wallet_backup}.new\" \"${wallet_backup}\"",
    user    => bitcoin,
    hour    => 1,
    minute  => 59,
  }

  # ensure we receive mail to bitcoin

  mailalias { 'bitcoin':
    recipient => $mailto,
  }

  # useful for processing the JSON output of bitcoin-cli

  package { 'jq': ensure => installed }

}
