# @summary
#   Manage a system's firewall.
#
# @example
#   include tails::profile::firewall
#
# @param purge
#   A Boolean indicating whether to clear any existing rules and make sure that
#   only rules defined in Puppet exist on the machine
#
# @param rules
#   A Hash containing firewall rules to be applied to this machine.
#
# @param public_services
#   A Hash containing description of public services that run in this node.
class tails::profile::firewall (
  Hash $rules = {},
  Hash $public_services = {},
) {
  include tirewall
  create_resources('firewall', $rules)
  create_resources('tirewall::public_service', $public_services)
}
