# profile for tor bridges

class tails::profile::bridge (
  String $relayname,
  String $email = lookup(tails::profile::sysadmins::email,undef,undef,'sysadmins@example.org'),
) {

# install packages

  # XXX Remove after system is upgraded to Bookworm
  $apt_pin_ensure = $::lsbdistcodename ? { 'bullseye' => 'present', default => 'absent' }
  $pkg_require = $::lsbdistcodename ? { 'bullseye' => Apt::Pin['obfs4proxy'], default => undef }

  apt::pin { 'obfs4proxy':
    ensure     => $apt_pin_ensure,
    packages   => ['obfs4proxy'],
    originator => 'Debian Backports',
    codename   => 'bullseye-backports',
    priority   => 991,
  }

  package { 'obfs4proxy':
    ensure  => installed,
    require => $pkg_require,
  }

  tor::daemon::relay { $relayname:
    address         => $::fqdn,
    port            => 9001,
    bridge_relay    => true,
    contact_info    => $email,
    bandwidth_rate  => 256,
    bandwidth_burst => 512,
  }
  tor::daemon::snippet { 'ExtORPort':
    content => "ExtORPort auto\n",
  }
  tor::daemon::snippet { 'ServerTransportPlugin':
    content => "ServerTransportPlugin obfs2,obfs3,obfs4 exec /usr/bin/obfs4proxy\n",
    require => Package['obfs4proxy'],
  }
  tor::daemon::snippet { 'ServerTransportListenAddr.obfs2':
    content => "ServerTransportListenAddr obfs2 0.0.0.0:16492\n",
  }
  tor::daemon::snippet { 'ServerTransportListenAddr.obfs3':
    content => "ServerTransportListenAddr obfs3 0.0.0.0:16493\n",
  }
  tor::daemon::snippet { 'ServerTransportListenAddr.obfs4':
    content => "ServerTransportListenAddr obfs4 0.0.0.0:16495\n",
  }

  # Publish services: Tor, obfs2, obfs3 and obfs4
  [ 9001, 16492, 16493, 16495 ].each | Integer $port | {
    tirewall::public_service { "tails::profile::bridge (${port})":
      dport => $port,
    }
  }

}
