# @summary
#   Manage systemd.
#
# @example
#   include tails::profile::systemd
#
# @param ntp_servers
#   An array containing NTP servers.
#
class tails::profile::systemd (
  Array $ntp_servers = ['0.pool.ntp.org', '1.pool.ntp.org'],
) {
  package { 'ntp': ensure => absent }

  ensure_packages(['systemd-timesyncd'])

  class { 'systemd':
    manage_timesyncd  => true,
    ntp_server        => $ntp_servers,
    manage_journald   => true,
    journald_settings => {
      'Storage'         => 'volatile',
      'MaxRetentionSec' => '5day',
    },
    require           => Package['systemd-timesyncd'],
  }
}
