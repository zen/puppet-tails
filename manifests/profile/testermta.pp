# Manage simple Postfix setup enabling nodes to send mail
class tails::profile::testermta (
  String $myhostname          = $::fqdn,
  String $myorigin            = $::fqdn,
  Array  $mydestination       = [ 'localhost' ],
  String $email_user          = "test@${::fqdn}",
) {

  class { 'postfix':
    parameters => {
      myhostname                            => $myhostname,
      myorigin                              => $myorigin,
      mydestination                         => $mydestination,
      mynetworks                            => [ '127.0.0.0/8' ],
      alias_maps                            => 'hash:/etc/aliases',
      alias_database                        => 'hash:/etc/aliases',
      always_add_missing_headers            => 'yes',
      append_dot_mydomain                   => 'no',
      biff                                  => 'no',
      bounce_queue_lifetime                 => '1w',
      compatibility_level                   => 2,
      default_destination_concurrency_limit => 2,
      default_destination_rate_delay        => '1s',
      delay_warning_time                    => '4h',
      disable_vrfy_command                  => 'yes',
      inet_interfaces                       => 'all',
      inet_protocols                        => 'ipv4',
      mailbox_size_limit                    => 0,
      maximal_queue_lifetime                => '1w',
      message_size_limit                    => 40960000,
      readme_directory                      => 'no',
      recipient_delimiter                   => '+',
      sender_canonical_maps                 => 'hash:/etc/postfix/maps/sender_canonical',
      show_user_unknown_table_name          => 'no',
      smtp_dns_support_level                => 'dnssec',
      smtp_tls_CApath                       => '/etc/ssl/certs',
      smtp_tls_ciphers                      => 'high',
      smtp_tls_loglevel                     => 1,
      smtp_tls_mandatory_protocols          => 'TLSv1.2',
      smtp_tls_mandatory_ciphers            => 'high',
      smtp_tls_mandatory_exclude_ciphers    => 'aNULL, MD5, DES, 3DES, DES-CBC3-SHA, RC4-SHA, AES256-SHA, AES128-SHA',
      smtp_tls_policy_maps                  => 'hash:/etc/postfix/maps/tls_policy',
      smtp_tls_protocols                    => 'TLSv1.2',
      smtp_tls_security_level               => 'dane',
      smtp_tls_session_cache_database       => "btree:\${data_directory}/smtp_scache",
      smtpd_banner                          => '$myhostname ESMTP $mail_name (Debian/GNU)',
      smtpd_client_connection_rate_limit    => 60,
      smtpd_client_message_rate_limit       => 120,
      smtpd_client_restrictions             => 'permit_mynetworks, permit_sasl_authenticated',
      smtpd_data_restrictions               => 'permit_mynetworks, permit_sasl_authenticated, reject_unauth_pipelining',
      smtpd_etrn_restrictions               => 'reject',
      smtpd_helo_required                   => 'yes',
      smtpd_helo_restrictions               => 'permit_mynetworks, permit_sasl_authenticated, reject_invalid_helo_hostname, reject_non_fqdn_helo_hostname, reject_unknown_helo_hostname',
      smtpd_recipient_restrictions          => 'permit_mynetworks, permit_sasl_authenticated, reject_non_fqdn_sender, reject_non_fqdn_recipient, reject_unauth_destination, reject_unknown_sender_domain, reject_unknown_recipient_domain, reject_invalid_hostname',
      smtpd_relay_restrictions              => 'permit_mynetworks, permit_sasl_authenticated, reject_non_fqdn_hostname, reject_non_fqdn_sender, reject_non_fqdn_recipient, reject_unauth_destination, reject_unknown_sender_domain, reject_unknown_recipient_domain, reject_invalid_hostname',
      smtpd_sasl_type                       => 'dovecot',
      smtpd_sasl_path                       => 'private/auth',
      smtpd_use_tls                         => 'yes',
      smtpd_tls_auth_only                   => 'yes',
      smtpd_tls_cert_file                   => '/etc/ssl/certs/ssl-cert-snakeoil.pem',
      smtpd_tls_key_file                    => '/etc/ssl/private/ssl-cert-snakeoil.key',
      smtpd_tls_session_cache_database      => "btree:\${data_directory}/smtpd_scache",
      virtual_transport                     => 'lmtp:unix:private/dovecot-lmtp',
      virtual_mailbox_domains               => $myorigin,
      virtual_mailbox_maps                  => 'hash:/etc/postfix/maps/vmailbox',
    },
    maps       => {
      tls_policy       => {
        type     => 'hash',
        contents => [
          '*          dane',
        ],
      },
      sender_canonical => {
        type     => 'hash',
        contents => [],
      },
      vmailbox         => {
        type     => 'hash',
        contents => [
          "${email_user}  whatever",
        ],
      },
    },
  }

  include tails::profile::canonical_senders

  postfix::config::service { 'submission':
    master_cf_file => '/etc/postfix/master.cf',
    type           => 'inet',
    priv           => 'n',
    chroot         => 'y',
    command        => 'smtpd',
    args           => [
      "-o syslog_name=postfix/submission",
      "-o smtpd_tls_security_level=encrypt",
      "-o smtpd_sasl_auth_enable=yes",
      "-o smtpd_reject_unlisted_recipient=no",
      "-o smtpd_recipient_restrictions=",
      "-o smtpd_relay_restrictions=permit_sasl_authenticated,reject",
      "-o milter_macro_daemon_name=ORIGINATING",
    ],
  }

  postfix::config::service { 'smtps':
    master_cf_file => '/etc/postfix/master.cf',
    type           => 'inet',
    priv           => 'n',
    chroot         => 'y',
    command        => 'smtpd',
    args           => [
      "-o syslog_name=postfix/smtps",
      "-o smtpd_tls_wrappermode=yes",
      "-o smtpd_sasl_auth_enable=yes",
      "-o smtpd_reject_unlisted_recipient=no",
      "-o smtpd_recipient_restrictions=",
      "-o smtpd_relay_restrictions=permit_sasl_authenticated,reject",
      "-o milter_macro_daemon_name=ORIGINATING",
    ],
  }

}
