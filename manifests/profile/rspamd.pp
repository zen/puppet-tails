# Manage Rspamd
class tails::profile::rspamd (
  Boolean       $dkim_signing = false,
  Array[String] $domains      = [],
) {

  include redis
  class { 'rspamd':
    manage_package_repo => false,
  }

  if $dkim_signing {

    rspamd::config {
      'dkim_signing:sign_local': value => true,
    }
    rspamd::config {
      'dkim_signing:sign_headers': value => '(o)from:(x)sender:(o)reply-to:(x)date:(x)message-id:(o)to:(o)cc:(x)content-type:(x)content-transfer-encoding:resent-to:resent-cc:resent-from:resent-sender:resent-message-id:(x)in-reply-to:(x)references:list-id:list-help:list-owner:list-unsubscribe:list-unsubscribe-post:list-subscribe:list-post:(x)openpgp:(x)autocrypt',
    }

    file { '/var/lib/rspamd/dkim':
      ensure  => directory,
      owner   => '_rspamd',
      group   => '_rspamd',
      mode    => '0700',
      require => Class['rspamd'],
    }

    $domains.each | String $signdomain | {
      exec { "/usr/bin/rspamadm dkim_keygen -s 'dkim' -d ${signdomain} -k /var/lib/rspamd/dkim/${signdomain}.dkim.key":
        user    => '_rspamd',
        group   => '_rspamd',
        require => File['/var/lib/rspamd/dkim'],
        creates => "/var/lib/rspamd/dkim/${signdomain}.dkim.key",
      }
    }
  }

}
