# Manage Schleuder and its MTA
class tails::profile::schleuder (
  String $superadmin_alias,
  String $myhostname       = 'lizard.tails.boum.org',
  String $myorigin         = 'mail.tails.boum.org',
  String $schleuder_domain = 'boum.org',
  Hash   $roles            = lookup(rbac::roles,undef,undef,{}),
  Hash   $users            = lookup(rbac::users,undef,undef,{}),
  Hash   $lists            = {},
  Hash   $sieved_lists     = {},
) {

  ### TODO
  #
  # - mta-sts
  #

  ### Sanity checks

  if $::lsbdistcodename != 'bullseye' {
    fail('The schleuder profile only supports Debian Bullseye.')
  }

  ### Dependencies

  package { 'ruby-base32': ensure => present }

  ### TLS

  file { "/etc/ssl/${myorigin}":
    ensure => directory,
    owner  => 'root',
    group  => 'root',
    mode   => '0500',
  }

  # Manage {cert,fullchain,privkey}.pem by collecting resources
  # exported by tails::mail::webserver
  File <<| tag == 'tails_mail_tls' |>>


  ### Postfix

  include tails::profile::rspamd

  class { 'postfix':
    parameters => {
      myhostname                            => $myhostname,
      myorigin                              => $myorigin,
      mydestination                         => [ $::fqdn, $myhostname, $myorigin, 'localhost' ],
      mynetworks                            => [ '127.0.0.0/8' ],
      alias_maps                            => 'hash:/etc/aliases',
      alias_database                        => 'hash:/etc/aliases',
      always_add_missing_headers            => 'yes',
      append_dot_mydomain                   => 'no',
      biff                                  => 'no',
      compatibility_level                   => 2,
      default_destination_concurrency_limit => 2,
      default_destination_rate_delay        => '1s',
      delay_warning_time                    => '4h',
      disable_vrfy_command                  => 'yes',
      inet_interfaces                       => 'all',
      inet_protocols                        => 'ipv4',
      mailbox_size_limit                    => 0,
      message_size_limit                    => 40960000,
      milter_default_action                 => 'accept',
      non_smtpd_milters                     => 'inet:localhost:11332',
      readme_directory                      => 'no',
      recipient_delimiter                   => '+',
      schleuder_destination_recipient_limit => 1,
      sender_canonical_maps                 => 'hash:/etc/postfix/maps/sender_canonical',
      show_user_unknown_table_name          => 'no',
      smtp_dns_support_level                => 'dnssec',
      smtp_tls_CApath                       => '/etc/ssl/certs',
      smtp_tls_ciphers                      => 'high',
      smtp_tls_loglevel                     => 1,
      smtp_tls_mandatory_protocols          => 'TLSv1.2',
      smtp_tls_mandatory_ciphers            => 'high',
      smtp_tls_mandatory_exclude_ciphers    => 'aNULL, MD5, DES, 3DES, DES-CBC3-SHA, RC4-SHA, AES256-SHA, AES128-SHA',
      smtp_tls_policy_maps                  => 'hash:/etc/postfix/maps/tls_policy',
      smtp_tls_protocols                    => 'TLSv1.2',
      smtp_tls_security_level               => 'dane',
      smtp_tls_session_cache_database       => "btree:\${data_directory}/smtp_scache",
      smtpd_banner                          => '$myhostname ESMTP $mail_name (Debian/GNU)',
      smtpd_client_auth_rate_limit          => 6,
      smtpd_client_connection_rate_limit    => 60,
      smtpd_client_message_rate_limit       => 120,
      smtpd_client_restrictions             => 'permit_mynetworks',
      smtpd_data_restrictions               => 'permit_mynetworks, reject_unauth_pipelining',
      smtpd_etrn_restrictions               => 'reject',
      smtpd_helo_required                   => 'yes',
      smtpd_helo_restrictions               => 'permit_mynetworks, reject_invalid_helo_hostname, reject_non_fqdn_helo_hostname, reject_unknown_helo_hostname',
      smtpd_milters                         => 'inet:localhost:11332',
      smtpd_recipient_restrictions          => 'permit_mynetworks, reject_non_fqdn_hostname, reject_non_fqdn_sender, reject_non_fqdn_recipient, reject_unauth_destination, reject_unknown_sender_domain, reject_unknown_recipient_domain, reject_invalid_hostname',
      smtpd_relay_restrictions              => 'permit_mynetworks, reject_non_fqdn_hostname, reject_non_fqdn_sender, reject_non_fqdn_recipient, reject_unauth_destination, reject_unknown_sender_domain, reject_unknown_recipient_domain, reject_invalid_hostname',
      smtpd_use_tls                         => 'yes',
      smtpd_tls_auth_only                   => 'yes',
      smtpd_tls_CAfile                      => "/etc/ssl/${myorigin}/fullchain.pem",
      smtpd_tls_cert_file                   => "/etc/ssl/${myorigin}/cert.pem",
      smtpd_tls_key_file                    => "/etc/ssl/${myorigin}/privkey.pem",
      smtpd_tls_session_cache_database      => "btree:\${data_directory}/smtpd_scache",
      transport_maps                        => 'hash:/etc/postfix/maps/transport',
      virtual_mailbox_domains               => $schleuder_domain,
    },
    maps       => {
      tls_policy       => {
        type     => 'hash',
        contents => [
          '*          dane',
        ],
      },
      sender_canonical => {
        type     => 'hash',
        contents => [],
      },
      transport        => {
        type     => 'hash',
        contents => [
          "${schleuder_domain} smtp:${schleuder_domain}\n",
        ],
      },
    },
  }

  postfix::config::service { 'schleuder':
    master_cf_file => '/etc/postfix/master.cf',
    command        => 'pipe',
    unpriv         => 'n',
    chroot         => 'n',
    args           => [ "flags=DRhu user=schleuder argv=/usr/bin/schleuder work \${recipient}" ],
  }

  postfix::config::service { 'dovecot':
    master_cf_file => '/etc/postfix/master.cf',
    command        => 'pipe',
    unpriv         => 'n',
    chroot         => 'n',
    args           => [ "flags=DRhu user=vmail:vmail argv=/usr/lib/dovecot/dovecot-lda -f \${sender} -d \${recipient}" ],
  }

  include tails::profile::canonical_senders


  ### Schleuder

  $superadmins = $roles['sysadmin']

  mailalias { $superadmin_alias: recipient => $superadmins }

  class { 'schleuder':
    cli_api_key   => sha1("${fqdn_rand(1204, 'cli')}"),
    gpg_use_tor   => true,
    superadmin    => $superadmin_alias,
    gpg_keyserver => 'hkps://keys.openpgp.org',
    require       => Package['ruby-base32'],
  }

  # create lists

  $all_lists = $lists + $sieved_lists

  keys($all_lists).each |String $list_name| {

    $admin_email = $users[$all_lists[$list_name]['admin']]['email']

    file { "/tmp/${list_name}-admin.asc":
      ensure  => present,
      content => $users[$all_lists[$list_name]['admin']]['pgpkey'],
      mode    => '0644',
    }

    schleuder::list { $list_name:
      admin           => $admin_email,
      admin_publickey => "/tmp/${list_name}-admin.asc",
      require         => [ Class['schleuder'], File["/tmp/${list_name}-admin.asc"] ],
    }

  }

  # set up postfix transports

  keys($lists).each |String $list_name| {
    ['', '-request', '-owner', '-bounce', '-sendkey'].each |String $suffix| {
      $address = join(split($list_name, '@'), "${suffix}@")
      concat::fragment { "postfix::map: list transport fragment ${address}":
        target  => '/etc/postfix/maps/transport',
        content => "${address} schleuder:\n",
      }
    }
  }

  # set up postfix transports and sieve filters

  keys($sieved_lists).each |String $list_name| {
    ['-request', '-owner', '-bounce', '-sendkey'].each |String $suffix| {
      $address = join(split($list_name, '@'), "${suffix}@")
      concat::fragment { "postfix::map: list transport fragment ${address}":
        target  => '/etc/postfix/maps/transport',
        content => "${address} schleuder:\n",
      }
    }
    concat::fragment { "postfix::map: list transport fragment ${list_name}":
      target  => '/etc/postfix/maps/transport',
      content => "${list_name} dovecot:\n",
    }
    $finaldest = join(split($list_name, '@'), '-final@')
    $finaltransport = split($list_name, '@')[0]
    concat::fragment { "postfix::map: list transport fragment ${finaldest}":
      target  => '/etc/postfix/maps/transport',
      content => "${finaldest} schleuder-final-${finaltransport}:\n",
    }
    postfix::config::service { "schleuder-final-${finaltransport}":
      master_cf_file => '/etc/postfix/master.cf',
      command        => 'pipe',
      unpriv         => 'n',
      chroot         => 'n',
      args           => [ "flags=DRhu user=schleuder argv=/usr/bin/schleuder work ${list_name}" ],
    }
    $subject = $sieved_lists[$list_name]['autoreply_subject']
    $content = $sieved_lists[$list_name]['autoreply_content']
    if has_key($sieved_lists[$list_name],'autoreply_customcondition') {
      $custom = $sieved_lists[$list_name]['autoreply_customcondition']
    }
    else {
      $custom = ''
    }
    if has_key($sieved_lists[$list_name],'autoreply_onlyto') {
      $onlyto = $sieved_lists[$list_name]['autoreply_onlyto']
      $addresses = ":addresses [\"${onlyto}\"] "
    }
    else {
      $addresses = ''
    }
    file { "/srv/mail/${list_name}":
      ensure => directory,
      mode   => '0700',
      owner  => 'vmail',
      group  => 'vmail',
    }
    file { "/srv/mail/${list_name}/.dovecot.sieve":
      ensure  => present,
      mode    => '0640',
      owner   => 'vmail',
      group   => 'vmail',
      content => "require [\"fileinto\", \"envelope\", \"vacation\"];\n${custom}\nvacation ${addresses}:subject \"${subject}\"\n  \"${content}\";\nredirect \"${finaldest}\";\n",
      require => File["/srv/mail/${list_name}"],
    }
  }

  ### Whitelist for automated messages

  $filters_dir = '/usr/local/lib/schleuder/filters'

  file { "${filters_dir}/pre_decryption":
    ensure => directory,
    owner  => 'root',
    group  => 'staff',
    mode   => '0755',
  }

  file { "${filters_dir}/pre_decryption/01_automated_messages_whitelist.rb":
    ensure  => present,
    source  => 'puppet:///modules/tails/schleuder/01_automated_messages_whitelist.rb',
    owner   => 'root',
    group   => 'staff',
    mode    => '0644',
    require => File["${filters_dir}/pre_decryption"],
    notify  => Service['schleuder-api-daemon'],
  }


  ### Dovecot (for sieve filtering)

  user { 'vmail':
    ensure     => present,
    home       => '/srv/mail',
    managehome => true,
    shell      => '/usr/sbin/nologin',
  }

  class { 'dovecot':
    plugins => ['sieve'],
    config  => {
      mail_uid      => 'vmail',
      mail_home     => '/srv/mail/%u',
      mail_location => 'maildir:~/Maildir',
    },
    configs => {
      '10-auth'  => {
        passdb => {
          driver => 'static',
          args   => 'nopassword=y',
        },
        userdb => {
          driver => 'static',
          args   => 'uid=vmail gid=vmail home=/srv/mail/%u',
        },
      },
      '15-lda'   => {
        'protocol lda' => {
          mail_plugins => '$mail_plugins sieve',
        },
      },
      '90-sieve' => {
        plugin => {
          sieve => 'file:/srv/mail/%u/sieve;active=/srv/mail/%u/.dovecot.sieve',
        },
      },
    },
  }

}
