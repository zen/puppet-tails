# Mirrorbits is a geographical download redirector.
#
# Successful deployment of this class will need the following manual steps:
#
#   - Configuration of GeoLite2 API key in:
#
#       tails::profile::mirrorbits::geolite2_api_key
#
#   - Configuration of an RPC password in:
#
#       tails::profile::mirrorbits::rpc_password
#
#   - Configuration of Fallback Mirrors in:
#
#       tails::profile::mirrorbits::fallbacks
#
# To-do:
#
#   - Run mirrorbits with non-root user
class tails::profile::mirrorbits (
  String $geolite2_apikey,
  String $rpc_password,
  String $repository                = '/srv/rsync/tails/tails',
  String $log_dir                   = '/var/log/mirrorbits',
  String $geoip_database_path       = '/usr/share/GeoIP/',
  String $listen_address            = ':8080',
  String $trace_file_location       = '/project/trace',
  Integer $repository_scan_interval = 5,
  Optional[Array] $fallbacks        = undef,
) {

  ### Sanity checks

  # Mirrorbits is currently only available in Bookworm...
  if $::lsbdistcodename !~ /^bullseye|bookworm$/ {
    fail('This class only supports Debian Bullseye and Bookworm.')
  }

  # ... so we need to make sure that the Bookworm repository is configured
  include tails::profile::apt

  ### Package

  # XXX Remove after system is upgraded to Bookworm
  $apt_pin_ensure = $::lsbdistcodename ? { 'bullseye' => 'present', default => 'absent' }
  $pkg_require = $::lsbdistcodename ? { 'bullseye' => Apt::Pin['mirrorbits'], default => undef }

  apt::pin { 'mirrorbits':
    ensure     => $apt_pin_ensure,
    packages   => [ 'mirrorbits', 'libc6', 'libc6-bin', 'libc-bin' ],
    originator => 'Debian',
    codename   => 'bookworm',
    priority   => 1000,
  }

  ensure_packages([ 'mirrorbits' ], { require => $pkg_require })

  ### Service

  systemd::unit_file { 'mirrorbits.service':
    source  => '/usr/share/doc/mirrorbits/examples/init/systemd/mirrorbits.service',
    require => Package['mirrorbits'],
  }

  ### GeoLite2 databases

  file { $geoip_database_path:
    ensure => directory,
    owner  => root,
    group  => root,
    mode   => '0755',
  }

  $update_dbs_executable = '/usr/local/sbin/update-geolite2-dbs'

  file { $update_dbs_executable:
    ensure  => 'file',
    source  => 'puppet:///modules/tails/profile/mirrorbits/update-geolite2-dbs.sh',
    owner   => 'root',
    group   => 'root',
    mode    => '0755',
    require => File[$geoip_database_path],
  }

  $geolite2_apikey_file = '/etc/mirrorbits-geolite2-api-key'

  file { $geolite2_apikey_file:
    ensure  => 'file',
    content => $geolite2_apikey,
    owner   => 'root',
    group   => 'root',
    mode    => '0600',
  }

  # Download GeoLite2 databases first time
  exec { 'Download GeoLite2 databases for the first time':
    command => $update_dbs_executable,
    user    => 'root',
    creates => '/usr/share/GeoIP/GeoLite2-Country.mmdb',
  }

  # Update the GeoLite2 database once a month
  cron { 'Update GeoLite2 databases':
    command  => $update_dbs_executable,
    user     => 'root',
    minute   => 0,
    hour     => 4,
    month    => '*',
    monthday => '1',
    weekday  => '*',
    require  => [
      File[$update_dbs_executable],
      File[$geolite2_apikey_file],
    ],
    notify   => Service['mirrorbits'],
  }

  ### Configuration

  file { $repository:
    ensure => 'directory',
    owner  => 'root',
    group  => 'root',
    mode   => '0755',
    notify => Service['mirrorbits'],
  }

  file { '/etc/mirrorbits.conf':
    ensure  => 'file',
    content => epp('tails/profile/mirrorbits/mirrorbits.conf.epp', {
      repository               => $repository,
      log_dir                  => $log_dir,
      geoip_database_path      => $geoip_database_path,
      listen_address           => $listen_address,
      rpc_password             => $rpc_password,
      repository_scan_interval => $repository_scan_interval,
      fallbacks                => $fallbacks,
      trace_file_location      => $trace_file_location,
    }),
    owner   => 'root',
    group   => 'root',
    notify  => Service['mirrorbits'],
    require => [
      File[$repository],
      File[$geoip_database_path],
    ]
  }

  ### Redis

  ensure_packages([ 'redis' ])

  service { 'redis':
    ensure  => 'running',
    enable  => true,
    require => Package['redis'],
  }

  ### Service

  service { 'mirrorbits':
    ensure  => 'running',
    enable  => true,
    require => [
      Systemd::Unit_file['mirrorbits.service'],
      File['/etc/mirrorbits.conf'],
      Package['mirrorbits'],
      Service['redis'],
    ],
  }

  ### Auto-update from `mirrors.json`

  ensure_packages([ 'python3' ])

  $update_mirrorbits_executable = '/usr/local/sbin/update-mirrorbits-mirrors'

  $rpc_password_file_ensure = $rpc_password ? { String => 'present', default => absent }

  file { '/etc/mirrorbits-rpc-password':
    ensure  => $rpc_password_file_ensure,
    content => $rpc_password,
    owner   => 'root',
    group   => 'root',
    mode    => '0600',
  }

  file { $update_mirrorbits_executable:
    ensure  => 'file',
    source  => 'puppet:///modules/tails/profile/mirrorbits/update-mirrorbits-mirrors.py',
    owner   => 'root',
    group   => 'root',
    mode    => '0755',
    require => [
      Package['python3'],
      File['/etc/mirrorbits-rpc-password']
    ],
  }

  cron { 'Update Mirrorbits':
    command => "curl -s https://tails.boum.org/mirrors.json | ${update_mirrorbits_executable}",
    user    => 'root',
    minute  => '*/15',
    require => [
      Service['mirrorbits'],
      File[$update_mirrorbits_executable],
    ],
  }

}
