# @summary
#   Ensure we can use augeas
#
# @example
#   include tails::profile::augeas
#
class tails::profile::augeas (
) {

  include augeas

  file { [
    '/opt/puppetlabs/puppet',
    '/opt/puppetlabs/puppet/cache',
    '/opt/puppetlabs/puppet/share',
    '/opt/puppetlabs/puppet/share/augeas',
  ]:
    ensure => directory,
    owner  => 'root',
    group  => 'root',
    mode   => '0755',
  }

}
