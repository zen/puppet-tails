# Manage a nginx vhost that reverse proxies to a Mirrorbits instance.
class tails::profile::mirrorbits::reverse_proxy (
  Stdlib::Fqdn $public_hostname   = 'mirrors.tails.boum.org',
  Stdlib::Fqdn $upstream_hostname = 'rsync.lizard',
  Stdlib::Port $upstream_port     = 8080,
  ) {

  $nginx_includename = 'tails_mirrorbits_reverse_proxy'
  $upstream_address = "${upstream_hostname}:${upstream_port}"

  nginx::vhostsd { $public_hostname:
    content => epp('tails/profile/mirrorbits/reverse_proxy.vhost.epp', {
      upstream_hostname => $upstream_hostname,
      upstream_address  => $upstream_address,
      public_hostname   => $public_hostname,
      nginx_includename => $nginx_includename,
    }),
    require => [
      Package[nginx],
      File['/etc/nginx/include.d/site_ssl.conf'],
      Nginx::Included[$nginx_includename],
      Tails::Letsencrypt::Certonly[$public_hostname],
    ];
  }

  nginx::included { $nginx_includename:
    content => template('tails/nginx/reverse_proxy.inc.erb'),
  }

  tails::letsencrypt::certonly { $public_hostname: }

}
