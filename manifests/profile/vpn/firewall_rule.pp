# @summary
#   A firewall rule specific to the VPN profile.
#
# @param params
#   A Hash passed as-is with parameters for configuring a firewall rule.
#
# @example
#   tails::profile::vpn::firewall_rule { '100 my-rule':
#     chain  => 'INPUT',
#     action => 'accept',
#   }
define tails::profile::vpn::firewall_rule (
  Hash $params,
) {

  firewall { $name:
    * => $params,
  }

}
