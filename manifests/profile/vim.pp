# @summary
#   Manage vim.
#
# @example
#   include tails::profile::vim
#
class tails::profile::vim (
) {
  include vim
}
