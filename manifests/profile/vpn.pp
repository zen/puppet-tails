# @summary
#   Configure a node's VPN connection
#
# @example
#   include tails::profile::vpn
#
# @param address
#   The node's IP address in the VPN.
#
# @param announce_routes
#   A Hash used to export network routes to the VPN. Each key is an identifier
#   and each corresponding value is a destination subnet.
#
# @param collect_routes
#   Either a Boolean or an Array[String] that indicates whether or which
#   network routes exported to the VPN should be collected in this node.
#   If `true`, all exported network routes are collected (except the ones
#   exported by this very node). If an Array[String] is passed, only routes
#   tagged with one of the items in the Array are collected.
class tails::profile::vpn (
  Stdlib::IP::Address::V4::Nosubnet $address,
  Array[String] $connect_to = [],
  Hash $announce_routes   = {},
  Variant[Boolean, Array[String]] $collect_routes = true,
  Optional[String] $proxy = undef,
) {

  $interface = 'tailsvpn'
  $_connect_to = $connect_to.filter | $target | { $target != $::hostname }

  tails::vpn::instance { "${::hostname}@${interface}":
    vpn_address    => "${address}/32",
    vpn_subnet     => '10.10.0.0/255.255.255.0',
    connect_to     => $_connect_to,
    proxy          => $proxy,
    collect_routes => $collect_routes,
  }

  $announce_routes.each | String $route_name, String $subnet | {
    @@concat::fragment { "${route_name} tinc-up ${interface}":
      target  => "/etc/tinc/${interface}/tinc-up",
      content => "# ${route_name}\nip route add ${subnet} via ${address} dev ${interface}\n",
      order   => 1,
      tag     => $trusted['certname'],
    }

    @@concat::fragment { "${route_name} tinc-down ${interface}":
      target  => "/etc/tinc/${interface}/tinc-down",
      content => "# ${route_name}\nip route del ${subnet} via ${address} dev ${interface}\n",
      order   => 1,
      tag     => $trusted['certname'],
    }

    @@tails::profile::vpn::firewall_rule { "300 accept routing to ${route_name} via ${interface} (exported by ${trusted['certname']})":
      params => {
        table       => 'filter',
        chain       => 'OUTPUT',
        destination => $subnet,
        outiface    => $interface,
        action      => 'accept',
      },
      tag    => $trusted['certname'],
    }
  }

}
