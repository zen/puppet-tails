# Manage a SMTP relay used by WhisperBack to send email to Tails help desk
class tails::profile::whisperback_onion (
  String $secret_key,
  String $public_key,
  String $onion_address = lookup(tails::profile::whisperback::onion_address,undef,undef,'')
) {

  package { 'swaks': ensure => installed }

  include tor
  tor::daemon::onion_service { 'tails_whisperback_relay':
    ports      => [ '25' ],
  }

  file { '/var/lib/tor/tails_whisperback_relay/hs_ed25519_secret_key':
    ensure  => present,
    owner   => 'debian-tor',
    group   => 'debian-tor',
    mode    => '0600',
    require => Tor::Daemon::Onion_service['tails_whisperback_relay'],
    content => base64('decode',$secret_key),
  }

  file { '/var/lib/tor/tails_whisperback_relay/hs_ed25519_public_key':
    ensure  => present,
    owner   => 'debian-tor',
    group   => 'debian-tor',
    mode    => '0600',
    require => File['/var/lib/tor/tails_whisperback_relay/hs_ed25519_secret_key'],
    content => base64('decode',$public_key),
  }

  file { '/var/lib/tor/tails_whisperback_relay/hostname':
    ensure  => present,
    owner   => 'debian-tor',
    group   => 'debian-tor',
    mode    => '0600',
    require => File['/var/lib/tor/tails_whisperback_relay/hs_ed25519_public_key'],
    content => "${onion_address}\n",
    notify  => Service['tor'],
  }

}
