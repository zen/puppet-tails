# Configure a libvirt host to allow Riseup's Munin service to connect and grab
# resource usage information.
class tails::profile::munin (
  Stdlib::IP::Address::V4 $munin_master = '127.0.0.1',
) {

  class { '::munin':
    allow => [ $munin_master ],
  }

  firewall { '100 accept Munin master':
    source => $munin_master,
    dport  => '4949',
    proto  => 'tcp',
    action => 'accept',
  }

  package { 'munin-libvirt-plugins':
    ensure => present
  }

  munin::plugin {
    [
      'libvirt-blkstat',
      'libvirt-mem',
      'libvirt-cputime',
      'libvirt-ifstat'
    ]:
    require => Package['munin-libvirt-plugins'],
  }

  # Start munin-node after libvirtd.service, otherwise plugins such as
  # libvirt-blkstat don't autoconfigure properly.
  systemd::dropin_file { 'munin-node.service.d/after-libvirtd.conf':
    unit     => 'munin-node.service',
    filename => 'after-libvirtd.conf',
    content  => "[Unit]\nAfter=libvirtd.service\n",
  }

}
