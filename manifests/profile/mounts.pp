class tails::profile::mounts (
  Hash $mounts = {},
) {

  $mounts.each | String $mountpoint, Hash $mountparams | {
    mount { "${mountpoint}":
      *      => $mountparams,
      before => Class['tails::profile::base'],
    }
  }

}
