# @summary
#   Manage sudo.
#
# @example
#   include tails::profile::sudo
#
class tails::profile::sudo (
) {
  class { 'sudo':
    config_file_replace => false,
  }

  sudo::conf {'group-sudo':
    content => '%sudo   ALL=(ALL:ALL) NOPASSWD : ALL',
  }
}
