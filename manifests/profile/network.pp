# @summary
#   Configure basic networking
#
# @example
#   include tails::profile::network
#
# @param masterless
#   Whether or not this node is running puppet masterless
#
# @param interfaces
#   A Hash used to declare network interface resources.
class tails::profile::network (
  Boolean $masterless = lookup(masterless,undef,undef,false),
  Hash $interfaces    = {},
) {

  service { 'networking':
    ensure => 'running',
  }

  class { 'network':
    config_file_notify => Service['networking'],
  }

  create_resources('network::interface', $interfaces)

# if we only have 1 interface defined here, we can be sure this is the
# IP we want to share for other hosts to add to their /etc/hosts file.

  unless $masterless {
    if $interfaces.length == 1 {
      $interfaces.each | String $iface, Hash $config | {
        @@host { "${::fqdn}":
          ip           => $config['ipaddress'],
          host_aliases => $hostname,
          tag          => 'hello_neighbour',
        }
      }
    }
  }
}
