# @summary
#   Install and configure libvirt
#
# @example
#   include tails::profile::libvirt
#
# @param huge_pages
#   If given, sets the number of huge pages in the kernel's huge pages pool.
#
# @param huge_pages_mountpoint
#   The mountpoint in case the host uses huge pages.
#
# @param qemu_security_driver
#   The security driver to use, 'unmanaged' will leave this undefined.
#
# @param qemu_custom_conf
#   A hash of key/value paris you want to put in the qemu.conf file.
#
class tails::profile::libvirt (
  String  $default_ip,
  String  $default_subnet,
  String  $default_netmask,
  String  $default_dhcp_start,
  String  $default_dhcp_end,
  String  $default_bridge        = 'virbr0',
  Optional[Integer] $huge_pages  = undef,
  String  $huge_pages_mountpoint = '/dev/hugepages',
  String  $qemu_security_driver  = 'unmanaged',
  Hash    $qemu_custom_conf      = {},
) {

  if $huge_pages {
    sysctl::value { 'vm.nr_hugepages': value => $huge_pages }
    mount { $huge_pages_mountpoint:
      ensure  => mounted,
      device  => hugetlbfs,
      fstype  => hugetlbfs,
      options => defaults,
    }
    $huge_pages_conf = { 'hugetlbfs_mount' => $huge_pages_mountpoint }
  } else {
    $huge_pages_conf = {}
  }

  if $qemu_security_driver != 'unmanaged' {
    $security_conf = { 'security_driver' => $qemu_security_driver }
  } else {
    $security_conf = {}
  }

  $qemu_conf = $huge_pages_conf + $security_conf + $qemu_custom_conf

  class { 'libvirt':
    qemu_conf => $qemu_conf,
  }

  libvirt_pool { 'default':
    ensure     => present,
    type       => 'logical',
    autostart  => true,
    sourcename => $hostname,
    target     => "/dev/${hostname}",
    require    => Class['libvirt'],
  }

  libvirt::network { 'default':
    forward_mode => 'open',
    bridge       => $default_bridge,
    ip_address   => $default_ip,
    ip_netmask   => $default_netmask,
    dhcp_start   => $default_dhcp_start,
    dhcp_end     => $default_dhcp_end,
  }

  class { 'tirewall::libvirt':
    bridge => $default_bridge,
    subnet => $default_subnet,
  }

  $packages = [
    virtinst,
  ]

  ensure_packages($packages)
}
