# @summary
#   Manage kernel parameters.
#
# @example
#   include tails::profile::sysctl
#
class tails::profile::sysctl (
) {
  sysctl::value { 'kernel.panic': value => 10 }
  sysctl::value { 'kernel.perf_event_paranoid': value => 2 }
  sysctl::value { 'kernel.unprivileged_bpf_disabled': value => 1 }
}
