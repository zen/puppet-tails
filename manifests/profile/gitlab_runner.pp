# A GitLab Runner service, including a local proxy/caching registry.
#
# Dependencies (versions are pinned for compatibility with Puppet 5.5):
#
#     # GitLab Runner
#     mod 'puppetlabs-apt', '7.7.1'
#     mod 'puppet-gitlab_ci_runner', '3.0.0'
#     mod 'puppetlabs-stdlib', '6.6.0'
#     
#     # Docker
#     mod 'puppetlabs-docker', '3.14.0'
#     mod 'puppetlabs-reboot', '3.2.0'
#     mod 'puppetlabs-translate', '2.2.0'
#     
# Use Hiera to add instances:
#
#     tails::profile::gitlab_runner::concurrent: 4
#     tails::profile::gitlab_runner::runners:
#       Docker@my-server: {}
#     tails::profile::gitlab_runner::runner_defaults:
#       url: https://gitlab.tails.boum.org/
#       registration-token: <secret>
#       executor: docker
#       docker-image: debian:bullseye
#
# TODO:
#
#   - Support rootless containers.
class tails::profile::gitlab_runner (
  Integer $concurrent = 4,
  Hash $runners = {},
  Hash $runner_defaults = {},
) {

  class { 'gitlab_ci_runner':
    concurrent      => $concurrent,
    manage_docker   => false,  # we want to pass our own defaults to the Docker class
    manage_repo     => false,
    runners         => $runners,
    runner_defaults => $runner_defaults,
  }

  # GitLab Runner is currently in Sid but not in Bullseye/Bookworm.
  # XXX Update when nodes are upgraded to a newer Debian version.
  $pin_ensure = $::lsbdistcodename ? { /^bullseye|bookworm$/ => 'present', default => 'absent' }

  apt::pin { 'gitlab-runner from Sid':
    ensure     => $pin_ensure,
    packages   => [ 'gitlab-runner' ],
    originator => 'Debian',
    codename   => 'sid',
    priority   => 990,
  }

  package { 'docker':
    ensure => 'installed',
    name   => 'docker.io',
  } -> Class['docker']

  file { '/usr/bin/dockerd':
    ensure  => 'link',
    target  => '/usr/sbin/dockerd',
    owner   => 'root',
    group   => 'root',
    require => Package['docker'],
  } -> Class['docker']

  class { 'docker':
    use_upstream_package_source => false,
    manage_package              => false,
    registry_mirror             => 'http://127.0.0.1:5000',
    extra_parameters            => [ '--insecure-registry=127.0.0.1:5000' ],
  }

  ### A proxy registry

  ensure_packages([ 'docker-registry' ])

  service { 'docker-registry':
    ensure => running,
  }

  file { '/etc/docker/registry/config.yml':
    ensure  => file,
    owner   => root,
    group   => root,
    mode    => '0644',
    source  => 'puppet:///modules/tails/gitlab_runner/docker-registry/config.yml',
    notify  => Service['docker-registry'],
    require => Package['docker-registry'],
  }

}
