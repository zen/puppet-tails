# Manage simple Postfix setup enabling nodes to send mail
class tails::profile::mta (
  String        $myhostname    = $::fqdn,
  String        $myorigin      = $::fqdn,
  Array         $mydestination = [ $::fqdn, $myhostname, $myorigin, 'localhost' ],
) {

  class { 'postfix':
    parameters => {
      myhostname                            => $myhostname,
      myorigin                              => $myorigin,
      mydestination                         => $mydestination,
      mynetworks                            => [ '127.0.0.0/8' ],
      alias_maps                            => 'hash:/etc/aliases',
      alias_database                        => 'hash:/etc/aliases',
      always_add_missing_headers            => 'yes',
      append_dot_mydomain                   => 'no',
      biff                                  => 'no',
      compatibility_level                   => 2,
      default_destination_concurrency_limit => 2,
      default_destination_rate_delay        => '1s',
      delay_warning_time                    => '4h',
      disable_vrfy_command                  => 'yes',
      inet_interfaces                       => 'all',
      inet_protocols                        => 'ipv4',
      mailbox_size_limit                    => 0,
      message_size_limit                    => 40960000,
      readme_directory                      => 'no',
      recipient_delimiter                   => '+',
      sender_canonical_maps                 => 'hash:/etc/postfix/maps/sender_canonical',
      show_user_unknown_table_name          => 'no',
      smtp_dns_support_level                => 'dnssec',
      smtp_tls_CApath                       => '/etc/ssl/certs',
      smtp_tls_ciphers                      => 'high',
      smtp_tls_loglevel                     => 1,
      smtp_tls_mandatory_protocols          => 'TLSv1.2',
      smtp_tls_mandatory_ciphers            => 'high',
      smtp_tls_mandatory_exclude_ciphers    => 'aNULL, MD5, DES, 3DES, DES-CBC3-SHA, RC4-SHA, AES256-SHA, AES128-SHA',
      smtp_tls_policy_maps                  => 'hash:/etc/postfix/maps/tls_policy',
      smtp_tls_protocols                    => 'TLSv1.2',
      smtp_tls_security_level               => 'dane',
      smtp_tls_session_cache_database       => "btree:\${data_directory}/smtp_scache",
      smtpd_banner                          => '$myhostname ESMTP $mail_name (Debian/GNU)',
      smtpd_client_connection_rate_limit    => 60,
      smtpd_client_message_rate_limit       => 120,
      smtpd_client_restrictions             => 'permit_mynetworks',
      smtpd_data_restrictions               => 'permit_mynetworks, reject_unauth_pipelining',
      smtpd_etrn_restrictions               => 'reject',
      smtpd_helo_required                   => 'yes',
      smtpd_helo_restrictions               => 'permit_mynetworks, reject_invalid_helo_hostname, reject_non_fqdn_helo_hostname, reject_unknown_helo_hostname',
      smtpd_recipient_restrictions          => 'permit_mynetworks, reject_non_fqdn_sender, reject_non_fqdn_recipient, reject_unauth_destination, reject_unknown_sender_domain, reject_unknown_recipient_domain, reject_invalid_hostname',
      smtpd_relay_restrictions              => 'permit_mynetworks, reject_non_fqdn_hostname, reject_non_fqdn_sender, reject_non_fqdn_recipient, reject_unauth_destination, reject_unknown_sender_domain, reject_unknown_recipient_domain, reject_invalid_hostname',
      smtpd_use_tls                         => 'no',
    },
    maps       => {
      tls_policy       => {
        type     => 'hash',
        contents => [
          '*          dane',
        ],
      },
      sender_canonical => {
        type     => 'hash',
        contents => [],
      },
    },
  }

  include tails::profile::canonical_senders

}
