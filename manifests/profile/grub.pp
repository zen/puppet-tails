# Set up custom GRUB configuration.

class tails::profile::grub (
  # GRUB options
  Array[String] $terminals          = ['serial'],
  String        $serial_command     = 'serial --speed=115200 --unit=1 --word=8 --parity=no --stop=1',
  # Linux command-line options
  Array[String] $consoles           = ['ttyS0,115200n8'],
  Boolean       $disable_ipv6       = $::virtual ? {
    physical => true,
    default  => false,
  },
  String        $elevator           = $::virtual ? {
    physical => '',
    default  => 'noop',
  },
  Array[String] $extra_boot_options = [],
) {

  $common_boot_options = [
    'apparmor=1',
    'security=apparmor',
  ]

  $elevator_boot_options = $elevator ? {
    ''      => [],
    default => ["elevator=${elevator}"],
  }

  $disable_ipv6_boot_options = $disable_ipv6 ? {
    true    => ['ipv6.disable=1'],
    default => [],
  }

  $console_boot_options = $consoles.map |$console| { "console=${console}" }

  $boot_options = join(
    $common_boot_options + $elevator_boot_options + $disable_ipv6_boot_options + $console_boot_options + $extra_boot_options,
    ' '
  )

  $set_grub_cmdline_linux = join([
    'set GRUB_CMDLINE_LINUX \'"', $boot_options, '"\'',
  ])

  $set_grub_terminal = join([
    'set GRUB_TERMINAL \'"', join($terminals, ' '), '"\'',
  ])

  $set_grub_serial_command = join([
    'set GRUB_SERIAL_COMMAND \'"', $serial_command, '"\'',
  ])

  exec { '/usr/sbin/update-grub': refreshonly => true }

  augeas { 'GRUB_configuration':
    context => '/files/etc/default/grub',
    changes => [
      $set_grub_cmdline_linux,
      $set_grub_terminal,
      $set_grub_serial_command,
    ],
    notify  => Exec['/usr/sbin/update-grub'],
    require => Package['apparmor'],
  }

}
