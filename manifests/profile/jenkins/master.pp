# jenkins master profile

class tails::profile::jenkins::master {
  rbac::user { 'ft-commit': }

  # TODO: refactoring
  include tails::jenkins::master
}
