# @summary
#   Manage a jenkins isotester.
#
# @example
#   include tails::profile::isotester
#
class tails::profile::jenkins::isotester (
) {
  file { [ '/etc/facter', '/etc/facter/facts.d' ]:
    ensure => directory,
    owner  => 'root',
    group  => 'root',
    mode   => '0755',
  }

  file { '/etc/facter/facts.d/role.yaml':
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    content => "---\nrole: isotester\n",
    require => File['/etc/facter/facts.d'],
  }

  @@sshkeys::create_key { "root@${::fqdn}":
    keytype => 'rsa',
    length  => 4096,
    tag     => 'ssh_keymaster',
  }

  include tails::jenkins::slave::iso_tester
}
