# @summary
#   Manage a jenkins worker.
#
# @example
#   include tails::profile::isoworker
#
class tails::profile::jenkins::isoworker (
) {
# set our role as fact to be able to access role-based hiera
  file { [ '/etc/facter', '/etc/facter/facts.d' ]:
    ensure => directory,
    owner  => 'root',
    group  => 'root',
    mode   => '0755',
  }

  file { '/etc/facter/facts.d/role.yaml':
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    content => "---\nrole: isoworker\n",
    require => File['/etc/facter/facts.d'],
  }

# install the necessary packages
  $packages = [
    'dnsmasq-base',
    'ebtables',
    'faketime',
    'git',
    'libvirt-daemon-system',
    'pigz',
    'qemu-system-x86',
    'qemu-utils',
    'rake',
    'sudo',
    'vagrant',
    'vagrant-libvirt',
    'vmdb2',
  ]

  ensure_packages($packages)

  @@sshkeys::create_key { "root@${::fqdn}":
    keytype => 'rsa',
    length  => 4096,
    tag     => 'ssh_keymaster',
  }

# TODO: refactor these
  include tails::jenkins::slave::iso_tester
  include tails::jenkins::slave::iso_builder
# For the build_website_* Jenkins jobs
  include tails::website::builder
# For the build_IUKs Jenkins job
  include tails::iuk_builder
}
