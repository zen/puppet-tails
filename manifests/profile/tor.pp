# @summary
#   Manage tor.
#
# @example
#   include tails::profile::tor
#
class tails::profile::tor (
) {
  include tor
}
