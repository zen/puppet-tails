# profile for mumble server

class tails::profile::mumble (
) {

  # install mumble and dependencies

  include mumble

  ensure_packages('sqlite3')

  # set up onion service

  tor::daemon::onion_service { 'mumble-hidden-v3':
    ports   => [ '64738' ],
  }

}
