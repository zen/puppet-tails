# oneshot puppet run

class tails::profile::puppet::agent {
  class { '::puppet':
    runmode                    => 'systemd.timer',
    systemd_randomizeddelaysec => 25 * 60,
    manage_packages            => false,
    agent                      => true,
    ssldir                     => '/var/lib/puppet/ssl',
    vardir                     => '/var/cache/puppet',
    sharedir                   => '/usr/share/puppet',
  }

  file { '/opt/puppetlabs':
    ensure => directory,
    owner  => 'root',
    group  => 'root',
    mode   => '0755',
  }
}
