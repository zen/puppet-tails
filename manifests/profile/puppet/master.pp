# profile for puppetmaster

class tails::profile::puppet::master (
) {
  file { [
    '/opt',
    '/opt/puppetlabs',
    '/opt/puppetlabs/server',
    '/opt/puppetlabs/server/apps',
    '/opt/puppetlabs/server/apps/puppetserver',
    '/etc/puppet/conf.d',
  ]:
    ensure => directory,
  }

# dirty hack to prevent the puppetmodule from resetting vardir ownership
# and restarting puppet master all the time

  exec { '/bin/chown root:root /var/cache/puppet': }

  class { 'puppet':
    manage_packages            => false,
    agent                      => true,
    runmode                    => 'systemd.timer',
    server                     => true,
    server_reports             => 'store,puppetdb',
    server_foreman             => false,
    server_external_nodes      => '',
    server_git_repo            => true,
    server_git_repo_path       => '/var/lib/puppet/puppet-code.git',
    server_storeconfigs        => true,
    server_puppetserver_dir    => '/etc/puppet',
    server_puppetserver_vardir => '/var/lib/puppet/server_data',
    server_puppetserver_rundir => '/var/run/puppet',
    server_puppetserver_logdir => '/var/log/puppet',
    ssldir                     => '/var/lib/puppet/ssl',
    vardir                     => '/var/cache/puppet',
    sharedir                   => '/usr/share/puppet',
    require                    => Exec['/bin/chown root:root /var/cache/puppet'],
  }

  class { 'puppet::server::puppetdb':
    server => $::fqdn,
  }

# ensure we have backups for the puppetdb

  include backupninja
  backupninja::pgsql { 'backup-puppetdb':
    ensure => present,
  }

# fixes for the messy debian puppet situation

  include tails::profile::puppet::puppetdb

# make sure we can encrypt sensitive data in hiera

  include tails::profile::puppet::eyaml

# ensure sysadmins can access the git repo

  rbac::ssh { 'sysadmins-to-puppet':
    user => 'puppet',
    role => 'sysadmin',
  }

# Clean up reports

  exec { '/usr/bin/find /var/lib/puppet/server_data/reports -type f -ctime +2 -exec /bin/rm {} \;': }

# Firewall

  tirewall::accept_trusted_subnets { 'Puppet Master':
    dport => 8140,
  }
}
