# PuppetDB is currently managed by theforeman/puppet plugin, but we need a bit
# of customization because of the PuppetDB package situation in Debian
class tails::profile::puppet::puppetdb () {
  # XXX PuppetDB is not available in Bullseye, so we install it from Sid
  $ensure = $::lsbdistcodename == 'bullseye' ? {
    true    => 'present',
    default => 'absent',
  }

  apt::pin { 'puppetdb':
    ensure   => $ensure,
    packages => 'puppetdb',
    codename => 'sid',
    priority => 1000,
  }

  # XXX Remove once Debian bug #990872 is fixed:
  #     https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=990872
  systemd::unit_file { 'puppetdb.service':
    ensure  => $ensure,
    content => '[Service]
ExecStart=
ExecStart=/usr/bin/java $JAVA_ARGS -Djava.security.egd=/dev/urandom \
  -XX:OnOutOfMemoryError="kill -9 %%p" \
  -cp /usr/share/puppetdb/puppetdb.jar:/usr/share/java/tools.analyzer.jar:/usr/share/java/tools.analyzer.jvm.jar:/usr/share/java/httpcore-nio.jar:/usr/share/java/trapperkeeper-scheduler.jar:/usr/share/java/quartz2.jar \
  clojure.main \
  -m puppetlabs.puppetdb.core \
  services \
  --config "/etc/puppetdb/conf.d" \
  --bootstrap-config "/etc/puppetdb/bootstrap.cfg" \
  --restart-file "/run/puppetdb/restart"',
  }
}
