class tails::profile::mirrorteam (
  String $apikey = lookup(tails::profile::dns_primary::pdns_api_key,undef,undef,''),
) {

  ensure_packages(['python3-requests','git'])

  user { 'tails_git':
    ensure         => present,
    system         => true,
    shell          => '/usr/bin/git-shell',
    home           => '/srv/repositories',
    purge_ssh_keys => true,
  }

  file { '/srv/repositories':
    ensure  => directory,
    owner   => 'tails_git',
    mode    => '2700',
    require => User['tails_git'],
  }

  rbac::ssh { 'mirrorteam-to-tails_git':
    user    => 'tails_git',
    role    => 'mirrorteam',
    require => File['/srv/repositories'],
  }

  vcsrepo { '/srv/repositories/mirrors.git':
    ensure   => bare,
    provider => 'git',
    user     => 'tails_git',
    require  => File['/srv/repositories'],
  }

  file { '/srv/repositories/mirrors.git/hooks/post-update':
    source  => 'puppet:///modules/tails/profile/mirrorteam/mirrors-post-update.hook',
    mode    => '0700',
    owner   => 'tails_git',
    require => [
      Vcsrepo['/srv/repositories/mirrors.git'],
      File['/usr/local/bin/update-dl.py'],
    ],
  }

  file { '/usr/local/bin/update-dl.py':
    owner   => 'tails_git',
    mode    => '0700',
    source  => 'puppet:///modules/tails/profile/mirrorteam/update-dl.py',
    require => [
      User['tails_git'],
      File['/etc/pdnsapi.secret'],
    ],
  }

  file { '/etc/pdnsapi.secret':
    owner   => 'tails_git',
    mode    => '0600',
    content => $apikey,
    require => User['tails_git'],
  }

}
