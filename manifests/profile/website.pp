# Profile for the TAILS website
class tails::profile::website (
) {
  include tails::website

  # grant UX workers access to the website logs

  user { 'www-data':
    ensure         => present,
    home           => '/var/www',
    managehome     => true,
    purge_ssh_keys => true,
    system         => true,
    shell          => '/bin/bash',
  }

  rbac::ssh { 'ux-to-www-data':
    user    => 'www-data',
    role    => 'ux',
    options => ['command="/usr/bin/rsync --server --sender -vlogDtprxe.iLsfxCIvu . /var/log/nginx"'],
  }

  tirewall::public_service { 'tails::profile::website HTTP':
    dport => 80,
  }

  tirewall::public_service { 'tails::profile::website HTTPS':
    dport => 443,
  }
}
