# Manage a rsync server
class tails::profile::rsync (
  Array $authusers,
  Stdlib::Absolutepath $basedir = '/srv/rsync',
  # Used by release managers during the release process
  Stdlib::Absolutepath $tmpdir  = '/srv/tmp',
  String $max_connections       = '23',
  ) {

  ### Sanity checks

  if $::operatingsystem != 'Debian' or versioncmp($::operatingsystemmajrelease, '11') < 0 {
    fail('This module only supports Debian 11 or newer.')
  }

  ### Variables

  $data_dir    = "${basedir}/tails"
  $secrets_dir = '/etc/rsync.d'
  $secrets     = "${secrets_dir}/tails.secrets"

  $auth_users = $authusers.map | $hash | { $hash['name'] }
  $authusersecrets = join($authusers.map | $hash | { join([$hash['name'],':',$hash['secret']]) },"\n")

  ### Resources

  package { 'acl': ensure => present }
  ensure_packages(['rsync'])

  user { 'rsync_tails':
    ensure     => present,
    system     => true,
    home       => '/home/rsync_tails',
    managehome => true,
  }

  file { [ $basedir, $secrets_dir ]:
    ensure => directory,
    owner  => root,
    group  => root,
    mode   => '0755',
  }

  file { [$data_dir, $tmpdir]:
    ensure  => directory,
    owner   => root,
    group   => rsync_tails,
    mode    => '2775',
    require => [ File[$basedir], User['rsync_tails'] ],
    notify  => Exec['set-acl-on-rsync-data-dir', 'set-acl-on-rsync-tmp-dir'],
  }

  file { '/etc/rsyncd.conf':
    content => template('tails/profile/rsync/rsyncd.conf.erb'),
    owner   => root,
    group   => root,
    mode    => '0600',
    notify  => Service[rsync],
  }

  file { $secrets:
    content => $authusersecrets,
    owner   => root,
    group   => 'rsync_tails',
    mode    => '0640',
    require => User['rsync_tails'],
  }

  service { 'rsync':
    ensure  => running,
    require => Package[rsync],
  }

  $acls = '/usr/local/etc/rsync-dir.acl'
  file { $acls:
    content => "group::rwX\ndefault:group::rwX\nother::r-X\ndefault:other::r-X\n",
    mode    => '0600',
    owner   => root,
    group   => root,
  }
  exec {
    'set-acl-on-rsync-data-dir':
      command     => "setfacl -R -M ${acls} ${data_dir}",
      require     => [ File[$acls, $data_dir], Package['acl'] ],
      subscribe   => File[$acls],
      refreshonly => true,
  }
  exec {
    'set-acl-on-rsync-tmp-dir':
      command     => "setfacl -R -M ${acls} ${tmpdir}",
      require     => [ File[$acls, $tmpdir], Package['acl'] ],
      subscribe   => File[$acls, $tmpdir],
      refreshonly => true,
  }

# RBAC

  rbac::user { 'ft-commit': }

  rbac::group { 'ft-commit-rsync_tails':
    group   => 'rsync_tails',
    role    => 'ft-commit',
    require => User['rsync_tails'],
  }

  sudo::conf {'rsync-group-sudo':
    content => '%rsync_tails   ALL=(ALL:ALL) NOPASSWD : ALL',
  }

# Firewall

  tirewall::public_service { 'tails::profile::rsync':
    dport => 873,
  }

}
