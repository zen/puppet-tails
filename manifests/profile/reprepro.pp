# Manage all Tails APT repositories
class tails::profile::reprepro (
  Hash $snapshots_architectures,
  String $signing_key,
  String $signing_key_content,
) {

# set up the webserver

  class { '::nginx':
    access_log => 'noip',
    error_log  => 'none'
  }

  # save disk space
  augeas { 'logrotate_nginx':
    context => '/files/etc/logrotate.d/nginx',
    changes => [
      'set rule/rotate 0',
      'rm rule/compress',
      'rm rule/delaycompress',
    ],
    require => Class['nginx'],
  }

# ensure we have all the plugins for proper monitoring

  include monitoring::plugins::number_in_file

# ensure we have the signing key available

  $gnupg_homedir = '/srv/reprepro/.gnupg'
  file { "${gnupg_homedir}/keys.asc":
    mode    => '0640',
    owner   => root,
    group   => reprepro,
    content => $signing_key_content,
    require => File[$gnupg_homedir],
  }

  file { '/srv/apt-snapshots/time-based/private-keys.asc':
    owner   => root,
    group   => 'reprepro-time-based-snapshots',
    mode    => '0440',
    content => $signing_key_content,
  }

# install and configure reprepro
# TODO: refactor the tails::reprepro manifests

  include reprepro

  class { '::tails::reprepro::custom':
    require => [ Class['::nginx'], Class['reprepro'] ],
  }

  class { 'tails::reprepro::snapshots::tagged': }

  class { 'tails::reprepro::snapshots::time_based':
    architectures => $snapshots_architectures,
    signwith      => $signing_key,
  }

# ensure access for ft members with commit rights

  rbac::user { 'ft-commit': }

  rbac::ssh { 'ft-commit-to-reprepro-time-based-snapshots':
    user    => 'reprepro-time-based-snapshots',
    role    => 'ft-commit',
    require => Class['tails::reprepro::snapshots::time_based'],
  }

  User <| title == 'reprepro' |> {
    purge_ssh_keys => true,
  }

  rbac::ssh { 'ft-commit-to-reprepro':
    user    => 'reprepro',
    role    => 'ft-commit',
    require => Class['reprepro'],
  }

}
