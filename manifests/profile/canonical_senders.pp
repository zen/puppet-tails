# Manage canonical sender addresses
class tails::profile::canonical_senders (
  Array $canonical_senders,
) {

  $canonical_senders.each |Hash $canonical_sender| {
    $user = $canonical_sender['user']
    $email = $canonical_sender['email']
    concat::fragment { "postfix::map: canonical senders fragment ${user}":
      target  => '/etc/postfix/maps/sender_canonical',
      content => "${user} ${email}\n",
      require => Class['postfix'],
    }
  }

}
