# @summary
#   Tweaks for virtual machines
#
# @example
#   include tails::profile::virtual
#
class tails::profile::virtual (
) {

# Remove smartmontools

  package { 'smartmontools': ensure => absent }

}
