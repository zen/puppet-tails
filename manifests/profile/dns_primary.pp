# Configure Tinc for primary DNS
class tails::profile::dns_primary (
  String $mysql_password,
  String $mysql_root_password,
  String $mysql_xerox_password,
  String $mysql_server_id,
  String $pdns_api_key,
) {

  class { 'powerdns':
    db_password           => $mysql_password,
    db_root_password      => $mysql_root_password,
    custom_repo           => true,
    backend_install       => false,
    backend_create_tables => false,
  }

  powerdns::config { 'gmysql-dnssec': }

  # don't use debian default pdns config
  file { '/etc/powerdns/pdns.d/pdns.local.gmysql.conf':
    ensure => absent,
  }

  class {'::mysql::server':
    root_password    => $mysql_root_password,
    override_options => {
      'mysqld' => {
        'bind_address'            => '0.0.0.0',
        'server_id'               => $mysql_server_id,
        'log-slave-updates'       => 'ON',
        'sync_binlog'             => 1,
        'log-bin'                 => '/var/log/mysql-bin/mysql-bin.log',
        'read_only'               => 'OFF',
        'binlog-format'           => 'ROW',
        'log-error'               => '/var/log/mysql/error.log',
        'innodb_buffer_pool_size' => '75M',
      },
    },
  }

  mysql_user { 'xerox@%':
    ensure        => 'present',
    password_hash => mysql_password($mysql_xerox_password)
  }

  mysql_grant { 'xerox@%/*.*':
    ensure     => 'present',
    privileges => ['REPLICATION SLAVE'],
    table      => '*.*',
    user       => 'xerox@%',
  }

  powerdns::config { 'api':
    ensure  => present,
    setting => 'api',
    value   => 'yes',
    type    => 'authoritative',
  }

  powerdns::config { 'api-key':
    ensure  => present,
    setting => 'api-key',
    value   => $pdns_api_key,
    type    => 'authoritative',
  }

  tirewall::public_service { 'tails::profile::dns_primary (TCP)':
    proto => 'tcp',
    dport => 53,
  }

  tirewall::public_service { 'tails::profile::dns_primary (UDP)':
    proto => 'udp',
    dport => 53,
  }

}
