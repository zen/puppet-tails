class tails::profile::mailalias (
  Array $aliases,
) {

  $aliases.each | Hash $alias | {
    $recipient = $alias['recipient']
    $addresses = $alias['addresses']
    $addresses.each | String $address | {
      mailalias { "${address}":
        recipient => $recipient,
      }
    }

  }

}
