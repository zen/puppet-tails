# @summary
#   Add hosts entries for our nodes
#
# @example
#   include tails::profile::hosts
#
# @param masterless
#   Whether or not this node is running puppet masterless
#
# @param default_hosts
#   A hash with all hosts entries, typically defined in common.yaml
#
# @param default_hosts
#   A hash adding to or overriding the default hosts
#
class tails::profile::hosts (
  Boolean $masterless = lookup(masterless,undef,undef,false),
  Hash $default_hosts = {},
  Hash $custom_hosts  = {},
) {
  $hosts = $default_hosts + $custom_hosts
  $hosts.each | String $host, Hash $params | {
    host { $host:
      * => $params,
    }
  }

  unless $masterless {
    Host <<| tag == 'hello_neighbour' |>>
  }
}
